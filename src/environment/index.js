/*
 * @file: index.js
 * @description: It Contain environment variables.
 * @date: 04.07.2018
 * @author: Pankaj Pandey
 */

const local = {
    apiUrl: 'https://reference-app-dev.herokuapp.com/',
};

const production = {
    apiUrl: 'https://easyreferencing.herokuapp.com/',
};

if (process.env.NODE_ENV === 'production') {
    module.exports = local;
} else {
    module.exports = local;
}