import React from "react";
// import Spinners from "../../images/1024.png";
// import Spinners from "../../images/loading_029.png";
import Spinners from "../../images/5226349.gif";
import { Spinner } from 'reactstrap';

const FullPageLoader = () => {
    return (<>
        <div className="">
            <div className="fp-container">
                <img src={Spinners} className="fp-loader" alt="loading" style={{ width: '6rem', height: '6rem' }} />
                {/* <p className=""><b>Loading...</b></p> */}
                {/* <Spinner type="grow" className="fp-loader" style={{ width: '6rem', height: '6rem' }} alt="loading" color="info" /> */}
            </div>
            {/* <div className="fp-container1">
                <p className="fp-loader1  text-center"><b>Loading...</b></p>
            </div> */}
        </div>
    </>
    );
};

export default FullPageLoader;