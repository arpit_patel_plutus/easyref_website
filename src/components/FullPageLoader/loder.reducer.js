import { SHOW_LODER, HIDE_LODER } from '../../actions/constants';

export default (state = {}, action) => {
    switch (action.type) {
        case HIDE_LODER:
            return { ...state, show_loder: '' };
        case SHOW_LODER:
            return { ...state, hide_loder: '' };
        default:
            return state;
    }
}