import React from "react";
import { Redirect } from "react-router-dom";

import Profile from "./profile/profile";
// import Navbar from './newsidebar/Navbar';

const withAuth = (Component) => {
    const isAuth = !!localStorage.getItem("user");
    if (isAuth) return <Profile><Component /></Profile>;
    else return <Redirect to="/login" />;
}

export default withAuth