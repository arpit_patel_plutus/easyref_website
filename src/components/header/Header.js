import React, { } from 'react';
import { Navbar, Col } from 'reactstrap';
import "./Header.scss";
import * as AiIcons from 'react-icons/ai';
import Apple from "../../images/apple.png";
import Google from "../../images/google.png";
const Header = (props) => {
    const login = !!localStorage.getItem("user");

    return (login ? loggedin() : notLoggedin())
    // return (
    //     <div>
    //         <Navbar className="header-bg positon_fix" light expand="md">
    //             {/* <Row > */}
    //             <Col lg={{ size: 8 }}>
    //                 <h3 className="text-white m-l-37">Easy Referencing</h3>
    //             </Col>
    //             {login ?
    //                 <Col className="text-center p-r-0" lg={{ size: 2 }} xs={{ size: 6 }}>
    //                     <h5 className="header_border text-white">
    //                         <span className="m-0 icon-size"><AiIcons.AiFillApple />
    //                         </span>
    //                         <span className="hide_text">Available on Appstore</span></h5>
    //                 </Col> : <Col className="text-center" lg={{ size: 2 }}>
    //                     <h5 className="header_border text-white"><span className="m-0 icon-size"><AiIcons.AiFillApple /></span><span className="hide_text">Available on Appstore</span></h5>
    //                 </Col>
    //             }
    //             {login ?
    //                 <Col className="text-center" lg={{ size: 2 }} xs={{ size: 1 }}>
    //                     <h5 className="header_border text-white"><span className="m-0 icon-size"><AiIcons.AiFillAndroid /></span><span className="hide_text">Available on Android</span></h5>
    //                 </Col> : <Col className="text-center" lg={{ size: 2 }} xs={{ size: 1 }}>
    //                     <h5 className="header_border text-white"><span className="m-0 icon-size"><AiIcons.AiFillAndroid /></span><span className="hide_text">Available on Android</span></h5>
    //                 </Col>
    //             }
    //             {login ? '' : <Col className="text-center p-r-0" lg={{ size: 2 }}>
    //                 <h5 className="header_border text-white">Login</h5>
    //             </Col>}
    //             {login ? '' : <Col className="text-center p-r-0" lg={{ size: 2 }}>
    //                 <h5 className="header_border text-white">Sign up</h5>
    //             </Col>}

    //             {/* </Row> */}
    //         </Navbar>
    //     </div>
    // );
}

const loggedin = () => {
    return <Navbar className="header-bg positon_fix easy-ref-header" light expand="md">
        <Col lg={{ size: 8 }}>
            <h3 className="logo">Easy Referencing</h3>
        </Col>
        {/* <Col className="p-0" lg={{ size: 2 }} xs={{ size: 6 }}>
                <a  className="header_border text-white" href="https://apps.apple.com/us/app/easy-referencing-citation/id1529091716">
                <img className="image_crop p-0" src={Apple}/>
                </a >
                <AiIcons.AiFillApple size="20" />
                <span className="hide_text">Available on Appstore</span>
        </Col>
        <Col className="p-0" lg={{ size: 2 }} xs={{ size: 6 }}>
            <a className="header_border text-white" href="https://play.google.com/store/apps/details?id=com.easyref.project&hl=en_IN&gl=US">
                <img className="image_crop p-0" src={Google} />
                <AiIcons.AiFillAndroid size="20" />
                <span className="hide_text">Available on Android</span>
            </a>
        </Col> */}
        <Col className="p-0 d-flex justify-content-end mobile-none" lg={{ size: 4 }}>
                <a  className="header_border text-white" href="https://apps.apple.com/us/app/easy-referencing-citation/id1529091716" target="_blank">
                    <img className="image_crop p-0" src={Apple}/>
                </a >
                {/* <AiIcons.AiFillApple size="20" />
                <span className="hide_text">Available on Appstore</span> */}
                <a className="header_border text-white ml-2" href="https://play.google.com/store/apps/details?id=com.easyref.project&hl=en_IN&gl=US" target="_blank">
                    <img className="image_crop p-0" src={Google} />
                    {/* <AiIcons.AiFillAndroid size="20" />
                    <span className="hide_text">Available on Android</span> */}
                </a>
        </Col> 
    </Navbar>
}

const notLoggedin = () => {
    return <Navbar className="header-bg positon_fix" light expand="md">
        <Col lg={{ size: 4 }}>
            <h3 className="text-white m-l-37">Easy Referencing</h3>
        </Col>
        <Col className="text-center" lg={{ size: 2 }}>
            <div className="header_border text-white"><AiIcons.AiFillApple size="20" /><span className="hide_text">Available on Appstore</span></div>
        </Col>
        <Col className="text-center" lg={{ size: 2 }} xs={{ size: 1 }}>
            <div className="header_border text-white"><AiIcons.AiFillAndroid size="20" /><span className="hide_text">Available on Android</span></div>
        </Col>
        <Col className="text-center p-r-0" lg={{ size: 2 }}>
            <h5 className="header_border text-white">Login</h5>
        </Col>
                <Col className="text-center p-r-0" lg={{ size: 2 }}>
            <h5 className="header_border text-white">Sign up</h5>
        </Col>
    </Navbar>
}

export default Header;