import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import { GET_STATS, SET_STATS } from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (user) => axios.get(`${apiUrl}users/stats/${user.id}`);
// const getcitation = () => axios.get(`${apiUrl}citation_styles/all`);

function* getStatesSaga({ user }) {
    try {
        const response = yield call(getUsersStats, user);
        if (response.data.errorCode === 0) {
            let total_ref = 0;
            response.data.data.forEach(pref => { total_ref += Number(pref.reference_count) });
            yield put({ type: SET_STATS, stats: response.data.data, total_ref });
        } else {
            // yield put({ type: types.USER_LOGIN_ERROR, response });
        }
    } catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(GET_STATS, getStatesSaga)]);
}

export default watchUserAuthentication;