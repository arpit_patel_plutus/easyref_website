import React, { Component, useState } from "react";
import { Col, Row, Button } from "reactstrap";
import { connect } from 'react-redux';
import { getStatsAction } from "../../actions/user/index";
import * as FaIcons from 'react-icons/fa';
import { useHistory } from "react-router-dom";
import * as types from '../../actions/constants';
import "./profile.scss";
import Header from "../../components/header/Header";
import Navbar from '../../components/newsidebar/Navbar';

const Image = {
    "Artwork": '../../images/icon/images/NewReferenceTypes/Artwork.png',
    "Book Review": '../../images/icon/images/NewReferenceTypes/BookReview.png',
    "Book": '../../images/icon/images/NewReferenceTypes/Book.png',
    "Map": '../../images/icon/images/NewReferenceTypes/Map.png',
    "Personal Communication": '../../images/icon/images/NewReferenceTypes/PersonalCommunication.png',
    "Presentation": '../../images/icon/images/NewReferenceTypes/Presentation.png',
    "Song": '../../images/icon/images/NewReferenceTypes/Song.png',
    "Speech": '../../images/icon/images/NewReferenceTypes/Speech.png',
    "Dictionary Entry": '../../images/icon/images/NewReferenceTypes/Dictionary.png',
    "Legal Case": '../../images/icon/images/NewReferenceTypes/Legal Case.png',
    "Blog Post": '../../images/icon/images/NewReferenceTypes/Blog Post.png',
    "Book Chapter": '../../images/icon/images/NewReferenceTypes/Book Chapter.png',
    "Conference Paper": '../../images/icon/images/NewReferenceTypes/Conference Paper.png',
    "Database": '../../images/icon/images/NewReferenceTypes/Database.png',
    "E-book": '../../images/icon/images/NewReferenceTypes/E-Book.png',
    "Encyclopedia Entry": '../../images/icon/images/NewReferenceTypes/Encyclopedia.png',
    "Film/Movie": '../../images/icon/images/NewReferenceTypes/FilmMovie.png',
    "Image": '../../images/icon/images/NewReferenceTypes/Image.png',
    "Interview": '../../images/icon/images/NewReferenceTypes/Interview.png',
    "Journal Article": '../../images/icon/images/NewReferenceTypes/Journal.png',
    "Legal Bill": '../../images/icon/images/NewReferenceTypes/Legal Bill.png',
    "Legislation": '../../images/icon/images/NewReferenceTypes/Legislation.png',
    "Magazine Article": '../../images/icon/images/NewReferenceTypes/Magazine.png',
    "News Article": '../../images/icon/images/NewReferenceTypes/News Article.png',
    "Patent": '../../images/icon/images/NewReferenceTypes/Patent.png',
    "Regulation": '../../images/icon/images/NewReferenceTypes/Regulation.png',
    "Report": '../../images/icon/images/NewReferenceTypes/Report.png',
    "Review": '../../images/icon/images/NewReferenceTypes/Review.png',
    "Standard": '../../images/icon/images/NewReferenceTypes/Standard.png',
    "Thesis/Dissertation": '../../images/icon/images/NewReferenceTypes/Thesis.png',
    "TV/Radio": '../../images/icon/images/NewReferenceTypes/TvRadio.png',
    "Video": '../../images/icon/images/NewReferenceTypes/Video.png',
    "Website": '../../images/icon/images/NewReferenceTypes/Website.png'
}

export class Sidebar extends Component {
    
    componentDidMount() {
        const { dispatch } = this.props;
        var user = localStorage.getItem("user");
        if (this.props.login.user)
            dispatch(getStatsAction(this.props.login.user));
        else
            var obj = JSON.parse(user);
        dispatch(getStatsAction(obj));
    }
    render() {
        const data = { ...this.props, ...this.state };
        return (<SidebarForm {...data} />);
    }

}

function getRandomColor() {
    var letters = '0123456789';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 10)];
    }
    return color;
}
const SidebarForm = (props) => {
    const history = useHistory();
    const { dispatch } = props;
    const [showProfile, setShowProfile] = useState(false);
    const onClick = () => setShowProfile(!showProfile);
    var name = localStorage.getItem("user");
    var obj = JSON.parse(name);

    function logout() {
        localStorage.clear();
        dispatch({ type: types.LOG_OUT, user: null });
        dispatch({ type: types.SHOW_LODER, isLoading: false });
        dispatch({ type: types.USER_LOGIN_ERROR, response: '' });
        history.push('/login');
    }
    return (
        <>
            <div className="profilesc ">
                <Row className={`m-0 profile-section ${showProfile ? '' : 'close'}`}>
                    <Col xs={{ size: 12 }} md={{ size: 8, offset: 2 }} className="p-r-0" >
                        <div className="header">
                            <h3>{obj.name}</h3>
                            <h6>Blimay you've made {props.total_ref} references</h6>
                            <h5>Stats:</h5>
                        </div>
                        <Row className='m-0 profile-cards'>
                            {props.stats && props.stats.map((card, randomColor) => (
                                <Col key={card.reference_type} className="profile-card" style={{ backgroundColor: getRandomColor() }} xs={{ size: 3 }} md={{ size: 1 }} sm={{ size: 3 }}>
                                    {
                                        Image[card.reference_type] ? (<div><img className="image_resize p-2" src={Image[card.reference_type]} /><br /><div className="set_word_image">{card.reference_count}</div></div>) : (<div className="set_word" >{card.reference_type}<br />{card.reference_count}</div>)
                                    }
                                </Col>
                            ))}
                        </Row>
                    </Col>
                    <Col xs={{ size: 12 }} md={{ size: 1, offset: 1 }} className="verticaly p-0">
                        <Button color="danger" onClick={logout} className="capsule vertical-center buttton_set">Sign out</Button>
                    </Col>
                    {/* <AiIcons.AiOutlineSetting className="setting" /> */}
                </Row>
                <div className="profile-on-off easy-ref-profile" onClick={onClick} ><FaIcons.FaRegUser /><div className={`arow_set ${showProfile ? 'open' : ''}`}>></div></div>
            </div>
            {/* <Row className='m-0'> */}
            <div> <Header /> </div>
            <div> <Navbar /> </div>
            {props.children}
            {/* </Row> */}
        </>
    )
};
// const actionCreators = {
//     logout: userActions.logout,
//     clear: alertActions.clear,
//     getUser: userActions.getUser,
//     uploadProfileImage: userActions.uploadProfileImage,
//     uploadCoverImage: userActions.uploadCoverImage,
//     getCurrentRoles: commonActions.getCurrentRoles,
//     updateUser: userActions.updateUser,
//     getCommonRegisterData: commonActions.getCommonRegisterData
// };
const mapStateToProps = (state) => {
    const { login, sidebar } = state;
    return { login, ...sidebar };
};

export default connect(mapStateToProps, null)(Sidebar);