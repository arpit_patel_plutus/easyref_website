import { GET_STATS, SET_STATS,LOG_OUT } from '../../actions/constants';

export default (state = {}, action) => {
    switch (action.type) {
        case GET_STATS:
            return { ...state, user: action.user };
        case SET_STATS:
            return { ...state, stats: action.stats, total_ref: action.total_ref };
        case LOG_OUT:
                return { ...state, user: null};
        default:
            return state;
    }
}