import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECT_ERROR,REFERENCELIST,REFERENCELISTGET ,SHOW_LODER,PROJECTNAME,REFERENCEALLDATA, REFERENCECREATE} from '../../actions/constants';
import { apiUrl } from '../../environment';



function* watchUserAuthentication() {
    yield all([takeLatest(REFERENCECREATE, getProjectlistSaga)]);
}

export default watchUserAuthentication;