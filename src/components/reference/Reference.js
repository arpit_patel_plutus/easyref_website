import React, { Component, useState } from "react";
import { Col, Row, Label, Input } from 'reactstrap';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as HiIcons from 'react-icons/hi';
import * as MdIcons from 'react-icons/md';
import {useDispatch, connect } from 'react-redux';
import { Dropdown, Modal, Button, Form,Accordion,Card } from 'react-bootstrap';
import BlogPost from "../../images/NewReferenceTypes/Blog Post.png";
import Book from "../../images/NewReferenceTypes/Book.png";
import BookChapter from "../../images/NewReferenceTypes/Book Chapter.png";
import BookReview from "../../images/NewReferenceTypes/BookReview.png";
import ConferencePaper from "../../images/NewReferenceTypes/Conference Paper.png";
import Dictionary from "../../images/NewReferenceTypes/Dictionary.png";
import Ebook from "../../images/NewReferenceTypes/E-Book.png";
import Encyclopedia from "../../images/NewReferenceTypes/Encyclopedia.png";
import FilmMovie from "../../images/NewReferenceTypes/FilmMovie.png";
import Image from "../../images/NewReferenceTypes/Image.png";
import Interview from "../../images/NewReferenceTypes/Interview.png";
import Journal from "../../images/NewReferenceTypes/Journal.png";
import LegalBill from "../../images/NewReferenceTypes/Legal Bill.png";
import LegalCase from "../../images/NewReferenceTypes/Legal Case.png";
import Legislation from "../../images/NewReferenceTypes/Legislation.png";
import Magazine from "../../images/NewReferenceTypes/Magazine.png";
import Map from "../../images/NewReferenceTypes/Map.png";
import NewsArticle from "../../images/NewReferenceTypes/News Article.png";
import Patent from "../../images/NewReferenceTypes/Patent.png";
import PersonalCommunication from "../../images/NewReferenceTypes/PersonalCommunication.png";
import Presentation from "../../images/NewReferenceTypes/Presentation.png";
import Regulation from "../../images/NewReferenceTypes/Regulation.png";
import Report from "../../images/NewReferenceTypes/Report.png";
import Review from "../../images/NewReferenceTypes/Review.png";
import Song from "../../images/NewReferenceTypes/Song.png";
import Speech from "../../images/NewReferenceTypes/Speech.png";
import Standard from "../../images/NewReferenceTypes/Standard.png";
import Thesis from "../../images/NewReferenceTypes/Thesis.png";
import TvRadio from "../../images/NewReferenceTypes/TvRadio.png";
import Video from "../../images/NewReferenceTypes/Video.png";
import Website from "../../images/NewReferenceTypes/Website.png";
import './reference.scss';
import { createReference } from "../../actions/user/index";
// import { MDBContainer, MDBCollapse, MDBCard, MDBCardBody, MDBCollapseHeader } from "mdbreact";


export class Reference extends Component {

    render() {

        const data = { ...this.props, ...this.state };
        return (<Ref {...data} />);

    }
}
const Ref = (props) => {
    const dispatch = useDispatch();
    const [show_ref, setShow_ref] = useState(false);
    const [Category_name, setCategory_name] = useState('');

    // const [show_edit, setShow_edit] = useState(true);
    // const handleClose_edit = () => setShow_edit(false);
    // const handleShow_edit = () => setShow_edit(true);

    const handleClose_ref = () => setShow_ref(false);
    const handleShow_ref = () => setShow_ref(true);
    const model_open = (name) => {
        setCategory_name(name)
        console.warn(name);
        props.handleShow_edit()
        handleShow_ref()
    }
    const [check_online, setcheck_online] = useState(false)
    const [inputList, setInputList] = useState([{ given: "", family: "", hide: true, literal: '' }]);

    // handle input change
    const handleInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...inputList];
        if (name === undefined) {
            list[index]['hide'] = !list[index]['hide'];
            list[index]['given'] = '';
            list[index]['family'] = '';
            list[index]['literal'] = '';
        }
        else {
            list[index][name] = value;
        }
        setInputList(list);
        console.log(list)
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    // console.log(hide)
    // handle click event of the Add button
    const handleAddClick = () => {
        setInputList([...inputList, { given: "", family: "", hide: true, literal: '' }]);
    }

    //Editor

    const [editorList, seteditorList] = useState([{ given: "", family: "", hide: true, literal: '' }]);

    // handle editor change
    const handleEditorChange = (e, index) => {
        const { name, value } = e.target;
        const editor = [...editorList];
        if (name === undefined) {
            editor[index]['hide'] = !editor[index]['hide'];
            editor[index]['given'] = '';
            editor[index]['family'] = '';
            editor[index]['literal'] = '';
        }
        else {
            editor[index][name] = value;
        }
        seteditorList(editor);

    };

    // handle click event of the Remove button
    const handleeditorRemoveClick = index => {
        const editor = [...editorList];
        if (editor.length === 1) {
            seteditor_show('d-none')
            seteditor_show_b('btn btn-info')
        }
        else {
            editor.splice(index, 1);

        }
        seteditorList(editor);
    };

    // handle click event of the Add button
    const handleeditorAddClick = () => {
        seteditorList([...editorList, { given: "", family: "", hide: true, literal: '' }]);
    }


    const [editor_show, seteditor_show] = useState('d-none')
    const [editor_show_b, seteditor_show_b] = useState('btn btn-info')

    const editorDisplay = () => {
        seteditor_show('d-block')
        seteditor_show_b('d-none')
    }


    // Translator
    const [trans_show, settrans_show] = useState('d-none')
    const [trans_show_b, settrans_show_b] = useState('btn btn-info ml-2')

    const transDisplay = () => {
        settrans_show('d-block')
        settrans_show_b('d-none')
    }
    const [transList, settransList] = useState([{ given: "", family: "", hide: true, literal: '' }]);

    // handle trans change
    const handletransChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...transList];
        if (name === undefined) {
            list[index]['hide'] = !list[index]['hide'];
            list[index]['given'] = '';
            list[index]['family'] = '';
            list[index]['literal'] = '';
            
        }
        else {
            list[index][name] = value;
        }
        settransList(list);

    };

    // handle click event of the Remove button
    const handletransRemoveClick = index => {
        const trans = [...transList];
        if (trans.length === 1) {
            settrans_show('d-none')
            settrans_show_b('btn btn-info ml-2')
        }
        else {
            trans.splice(index, 1);

        }
        settransList(trans);
    };

    // console.log(hide)
    // handle click event of the Add button
    const handletransClick = () => {
        settransList([...transList, { given: "", family: "", hide: true, literal: '' }]);
    }

    // additional
    const [additionalList, setadditionalList] = useState([{ given: "", family: "", hide: true, literal: '' }]);

    // handle additional change
    const handleadditionalChange = (e, index) => {
        const { name, value } = e.target;
        const additional = [...additionalList];
        if (name === undefined) {
            additional[index]['hide'] = !additional[index]['hide'];
            additional[index]['given'] = '';
            additional[index]['family'] = '';
            additional[index]['literal'] = '';
        }
        else {
            additional[index][name] = value;
        }
        setadditionalList(additional);
    };

    // handle click event of the Remove button
    const handleadditionalRemoveClick = index => {
        const additional = [...additionalList];
        additional.splice(index, 1);
        setadditionalList(additional);
    };

    // console.log(hide)
    // handle click event of the Add button
    const handleadditionalAddClick = () => {
        setadditionalList([...additionalList, { given: "", family: "", hide: true, literal: '' }]);
    }
    const [colleapse , setColleapse] = useState(true)
    const [colleapse_main, setColleapseMain] = useState(true)
    const [colleapse_annotation, setColleapseAnnotation] = useState(true) 
    const [formData, updateFormData] = useState([]);

    const handleChange = (e) => {
        updateFormData({
          ...formData,
    
          // Trimming any whitespace
          [e.target.name]: e.target.value.trim()
        });
      };
    const ReferenceDone = (e) =>
    {
        e.preventDefault();
        // console.warn(formData);
        if(Category_name == 'Blog Post')
        {

            var Date_Published= formData.Date_Published.split("-")
            var Date_Accessed= formData.Date_Accessed.split("-")
            var Data= {
                "id":"item-1",
                "type":"post-weblog",
                "author":inputList,
                "editor":editorList,
                "title":formData.Title_of_Post,
                "issued":{"date-parts":[Date_Published]},
                "URL":formData.url,
                "accessed":{"date-parts":[Date_Accessed]},
                "container-title" : formData.Blog_Title,
                "Annote":formData.Annotation
            }
            var ref_type = 'blog post'

            dispatch(createReference(props.project_ids,ref_type,Data))
        }
        setInputList([{ given: "", family: "", hide: true, literal: '' }]);
        seteditorList([{ given: "", family: "", hide: true, literal: ''}]);
        settransList([{ given: "", family: "", hide: true, literal: ''}]);
        setadditionalList([{ given: "", family: "", hide: true, literal: ''}]);
    }
    console.warn(props);
    return (
        <>
            <Modal className="modal_width" show={props.ref_model}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered onHide={() => props.handleShow_edit()}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        New Reference
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ul className="remove_dot p-0 height_set hover_class">
                        <li style={{ background: '#003663', fontSize: "20px", color: "white" }} onClick={() => model_open('Blog Post')}><img className="image_size_cate p-2" src={BlogPost} /> Blog Post</li>

                        <li style={{ background: '#045fac', fontSize: "20px", color: "white" }} onClick={() => model_open('Book')}><img className="image_size_cate p-2" src={Book} /> Book</li>

                        <li style={{ background: '#008ab6', fontSize: "20px", color: "white" }} onClick={() => model_open('Book Chapter')}><img className="image_size_cate p-2" src={BookChapter} /> Book Chapter</li>

                        <li style={{ background: '#07aade', fontSize: "20px", color: "white" }} onClick={() => model_open('Book Review')}><img className="image_size_cate p-2" src={BookReview} /> Book Review</li>

                        <li style={{ background: '#5020d7', fontSize: "20px", color: "white" }} onClick={() => model_open('Conference Paper')}><img className="image_size_cate p-2" src={ConferencePaper} /> Conference Paper</li>

                        <li style={{ background: '#4a46ff', fontSize: "20px", color: "white" }} onClick={() => model_open('Dictionary Entry')}><img className="image_size_cate p-2" src={Dictionary} /> Dictionary Entry</li>

                        <li style={{ background: '#7875fe', fontSize: "20px", color: "white" }} onClick={() => model_open('E-book')}><img className="image_size_cate p-2" src={Ebook} /> E-book</li>

                        <li style={{ background: '#b6b5f7', fontSize: "20px", color: "white" }} onClick={() => model_open('Encyclopedia Entry')}><img className="image_size_cate p-2" src={Encyclopedia} /> Encyclopedia Entry</li>

                        <li style={{ background: '#0cd350', fontSize: "20px", color: "white" }} onClick={() => model_open('Film Movie')}><img className="image_size_cate p-2" src={FilmMovie} /> Film Movie</li>

                        <li style={{ background: '#086528', fontSize: "20px", color: "white" }} onClick={() => model_open('Image')}><img className="image_size_cate p-2" src={Image} />Image</li>

                        <li style={{ background: '#34aa68', fontSize: "20px", color: "white" }} onClick={() => model_open('Interview')}><img className="image_size_cate p-2" src={Interview} />Interview</li>

                        <li style={{ background: '#80e5b7', fontSize: "20px", color: "white" }} onClick={() => model_open('Journal')}><img className="image_size_cate p-2" src={Journal} />Journal Article</li>

                        <li style={{ background: '#E8D548', fontSize: "20px", color: "white" }} onClick={() => model_open('Legal Bill')}><img className="image_size_cate p-2" src={LegalBill} />Legal Bill</li>

                        <li style={{ background: '#DAB53C', fontSize: "20px", color: "white" }} onClick={() => model_open('Legal Case')}><img className="image_size_cate p-2" src={LegalCase} />Legal Case</li>

                        <li style={{ background: '#AA862B', fontSize: "20px", color: "white" }} onClick={() => model_open('Legislation')}><img className="image_size_cate p-2" src={Legislation} />Legislation</li>

                        <li style={{ background: '#DF974A', fontSize: "20px", color: "white" }} onClick={() => model_open('Magazine Article')}><img className="image_size_cate p-2" src={Magazine} />Magazine Article</li>

                        <li style={{ background: '#E07708', fontSize: "20px", color: "white" }} onClick={() => model_open('Map')}><img className="image_size_cate p-2" src={Map} />Map</li>

                        <li style={{ background: '#E45926', fontSize: "20px", color: "white" }} onClick={() => model_open('News Article')}><img className="image_size_cate p-2" src={NewsArticle} />News Article</li>

                        <li style={{ background: '#E33D28', fontSize: "20px", color: "white" }} onClick={() => model_open('Patent')}><img className="image_size_cate p-2" src={Patent} />Patent</li>

                        <li style={{ background: '#C21B05', fontSize: "20px", color: "white" }} onClick={() => model_open('Personal Communication')}><img className="image_size_cate p-2" src={PersonalCommunication} />Personal Communication</li>

                        <li style={{ background: '#8F1303', fontSize: "20px", color: "white" }} onClick={() => model_open('Regulation')}><img className="image_size_cate p-2" src={Regulation} />Regulation</li>

                        <li style={{ background: '#600D02', fontSize: "20px", color: "white" }} onClick={() => model_open('Report')}><img className="image_size_cate p-2" src={Report} />Report</li>

                        <li style={{ background: '#CB1C48', fontSize: "20px", color: "white" }} onClick={() => model_open('Review')}><img className="image_size_cate p-2" src={Review} />Review</li>

                        <li style={{ background: '#E43899', fontSize: "20px", color: "white" }} onClick={() => model_open('Song')}><img className="image_size_cate p-2" src={Song} />Song</li>

                        <li style={{ background: '#C617AB', fontSize: "20px", color: "white" }} onClick={() => model_open('Speech')}><img className="image_size_cate p-2" src={Speech} />Speech</li>

                        <li style={{ background: '#770C67', fontSize: "20px", color: "white" }} onClick={() => model_open('Standard')}><img className="image_size_cate p-2" src={Standard} />Standard</li>

                        <li style={{ background: '#730989', fontSize: "20px", color: "white" }} onClick={() => model_open('Thesis Dissertation')}><img className="image_size_cate p-2" src={Thesis} />Thesis Dissertation</li>

                        <li style={{ background: '#4F0773', fontSize: "20px", color: "white" }} onClick={() => model_open('TV Radio')}><img className="image_size_cate p-2" src={TvRadio} />TV Radio</li>

                        <li style={{ background: '#33054A', fontSize: "20px", color: "white" }} onClick={() => model_open('Video')}><img className="image_size_cate p-2" src={Video} />Video</li>

                        <li style={{ background: '#25023A', fontSize: "20px", color: "white" }} onClick={() => model_open('Website')}><img className="image_size_cate p-2" src={Website} />Website</li>

                    </ul>
                </Modal.Body>
            </Modal>
            <Modal show={show_ref} className="reference-modal" onHide={handleClose_ref} animation={false} size="lg" aria-labelledby="contained-modal-title-vcenter" centered onHide={handleClose_ref}>
                <Modal.Header className="header_body" closeButton>
                    <Modal.Title className="header_title">{Category_name}</Modal.Title>
                </Modal.Header>
                <form onSubmit={(values) => ReferenceDone(values)}>
                    <Modal.Body>
                        {
                            Category_name == 'Book Review' ? <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref">
                                    <h4 className="modal-main-title">Review Contributors</h4>
                                </Col>
                            </Row>
                                : ''
                        }
                        {
                            Category_name == 'Book Chapter' ? <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref">
                                    <h4 className="modal-main-title">Chapter Contributors</h4>
                                </Col>
                            </Row> : ''
                        }
                        {
                            Category_name == 'Review' ? <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref">
                                    <h4 className="modal-main-title">Reviewers</h4>
                                </Col>
                            </Row> : ''
                        }
                        {
                            Category_name != 'Book Review' && Category_name != 'Book Chapter' && Category_name != 'Review' ? <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref">
                                    <h4 className="modal-main-title">Contributors</h4>
                                </Col>
                            </Row> : ''
                        }
                        {
                            Category_name == 'Blog Post' ? <> 
                            <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Post Author</h4>
                                </Col>
                            </Row>
                                <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="mb-0"> 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }
                                        <div className="">
                                            {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                            {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    </div>
                                )}
                                </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} >
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                    {
                                        editorList.map((x, i) =>
                                                <div className={editor_show}> 
                                                    {x.hide ?
                                                        <Row className="m_b_10" >
                                                            <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                                {
                                                                    editor_show == 'd-block' ? <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required /> : <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)}  />
                                                                }
                                                                

                                                            </Col>
                                                            <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                                {
                                                                   editor_show == 'd-block' ? <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required /> : <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" />
                                                                }
                                                                
                                                            </Col>
                                                            <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                                <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                                <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                            </Col>
                                                            <br></br>
                                                        </Row> :
                                                        <Row className="m_b_10" >
                                                            <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                                <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                            </Col>
                                                            <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                                <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                                <AiIcons.AiFillDelete color="#ff2d2d" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                            </Col>
                                                        </Row>
                                                    }
                                                    {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                                    {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                                </div>
                                        )
                                    }
                                </div>
                            </>
                                : ''
                        }
                        {
                            Category_name == 'Book' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Book Author</h4>
                                </Col>
                            </Row>
                                <div className="mb-4">
                                    {inputList.map((x, i) =>
                                        <div className="mb-0" >
                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                        {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                    </Col>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                        {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                    </Col>
                                                </Row>
                                            }

                                            {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                            {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                            {inputList.length - 1 === i && <button type="button" className={trans_show_b} onClick={() => transDisplay()}>Add Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                        </div>
                                    )}
                                </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} >
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                                <Row className={trans_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={trans_show}>
                                        <h4 className="modal-sub-title">Translator</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    transList.map((x, i) =>
                                        <div className={trans_show} >
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handletransChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {transList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handletransClick}>Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )}
                                    </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Book Chapter' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Chapter Author</h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="mb-0"> 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={trans_show_b} onClick={() => transDisplay()}>Add Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                    </div>
                                )}
                            </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >
                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )}
                                <Row className={trans_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={trans_show}>
                                        <h4 className="modal-sub-title">Translator</h4>
                                    </Col>
                                </Row>
                                {
                                    transList.map((x, i) =>
                                        <div className={trans_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handletransChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {transList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handletransClick}>Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Book Review' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Review Author  </h4>
                                </Col>
                            </Row>
                                <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="mb-0" >

                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                    </div>
                                )}
                                </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Conference Paper' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Paper Author  </h4>
                                </Col>
                            </Row>
                                <div className="mb-4">
                                    {inputList.map((x, i) =>
                                        <div className="mb-0" > 
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                        {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                    </Col>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                        {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                    </Col>
                                                </Row>
                                            }

                                            {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                            {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                        </div>
                                    )}
                                </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} > 
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Dictionary Entry' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Entry Author  </h4>
                                </Col>
                            </Row>
                                <div className="mb-4">
                                    {inputList.map((x, i) =>
                                        <div className="mb-0"> 
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                        {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                    </Col>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                        {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                    </Col>
                                                </Row>
                                            }

                                            {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                            {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                        </div>
                                    )}
                                </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} > 
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )}
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'E-book' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Author  </h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="mb-0"> 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        } 
                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={trans_show_b} onClick={() => transDisplay()}>Add Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                    </div>
                                )}
                            </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                    {
                                        editorList.map((x, i) =>
                                            <div className={editor_show} >

                                                {x.hide ?
                                                    <Row className="m_b_10" >
                                                        <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                            <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                        </Col>
                                                        <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                            <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                        </Col>
                                                        <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                            <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                            <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                        </Col>
                                                        <br></br>
                                                    </Row> :
                                                    <Row className="m_b_10" >
                                                        <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                            <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                        </Col>
                                                        <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                            <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                            <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                        </Col>
                                                    </Row>
                                                }

                                                {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                                {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                            </div>
                                        )
                                    }
                                </div>
                                <Row className={trans_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={trans_show}>
                                        <h4 className="modal-sub-title">Translator</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    transList.map((x, i) =>
                                        <div className={trans_show} >

                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handletransChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {transList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handletransClick}>Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Encyclopedia Entry' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Entry Author  </h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={trans_show_b} onClick={() => transDisplay()}>Add Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                    </div>
                                )}
                            </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                                <Row className={trans_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={trans_show}>
                                        <h4 className="mobile-sub-title">Translator</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    transList.map((x, i) =>
                                        <div className={trans_show} >

                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handletransChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handletransChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handletransChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handletransRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {transList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handletransClick}>Translator <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Film Movie' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Director</h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="mb-0" > 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Director <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                    </div>
                                )}
                            </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} > 
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Image' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Illustrator or Copyright Holder  </h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="" > 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Illustrator <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                    </div>
                                )}
                            </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} > 
                                            {x.hide ?
                                                <Row className="m_b_10">
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10">
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Interview' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Interviewee</h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="" > 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleAddClick}>Add Interviewee <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                    </div>
                                )
                            }
                            </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Journal' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Article Author</h4>
                                </Col>
                            </Row>
                            <div className="mb-4">
                                {inputList.map((x, i) =>
                                    <div className="mb-0"> 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10">
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }
                                        {inputList.length - 1 === i && <button type="button" className="btn btn-info mr-2" onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        {inputList.length - 1 === i && <button type="button" className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                    </div>
                                )}
                            </div>
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="modal-sub-title">Editor</h4>
                                    </Col>
                                </Row>
                                <div className="mb-4">
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >
                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                        </div>
                                    )
                                }
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Legal Bill' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Sponsor </h4>
                                </Col>

                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleAddClick}>Add Sponsor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                            </> : ''
                        }
                        {
                            Category_name == 'Legal Case' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Case Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                            </> : ''
                        }
                        {
                            Category_name == 'Legislation' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Legislation Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                            </> : ''
                        }
                        {
                            Category_name == 'Magazine Article' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Article Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Map' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Map Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'News Article' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Article Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Patent' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Inventor  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleAddClick}>Add Inventor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Personal Communication' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Sender/Speaker/Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                            </> : ''
                        }
                        {
                            Category_name == 'Regulation' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Regulation Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >
                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Report' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Report Author </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Review' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Review Author </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Song' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Artist </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleAddClick}>Add Artist <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Speech' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Speaker </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleAddClick}>Add Speaker <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Standard' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Thesis Dissertation' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'TV Radio' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Director or Presenter </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleAddClick}>Add Director <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Video' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Author  </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }
                        {
                            Category_name == 'Website' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Website Author </h4>
                                </Col>
                            </Row>
                                {inputList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleInputChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleInputChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleInputChange(e, i)} />
                                                    {inputList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {inputList.length - 1 === i && <p className="btn btn-info m-t-10 " onClick={handleAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        {inputList.length - 1 === i && <p className={editor_show_b} onClick={() => editorDisplay()}>Add Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                                <Row className={editor_show}>
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className={editor_show}>
                                        <h4 className="align-items-center m-t-0">Editor</h4>
                                    </Col>
                                </Row>
                                {
                                    editorList.map((x, i) =>
                                        <div className={editor_show} >

                                            {x.hide ?
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleEditorChange(e, i)} required />

                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                    <br></br>
                                                </Row> :
                                                <Row className="m_b_10 m-t-10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleEditorChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleEditorChange(e, i)} />
                                                        <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleeditorRemoveClick(i)} />
                                                    </Col>
                                                </Row>
                                            }

                                            {/* {inputList.length - 1 === i && <p className="btn btn-info m-t-10 m-l-15" onClick={handleeditorAddClick}>Add Author <AiIcons.AiOutlinePlusCircle color="white" /></p>} */}
                                            {editorList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleeditorAddClick}>Editor <AiIcons.AiOutlinePlusCircle color="white" /></p>}
                                        </div>
                                    )}
                            </> : ''
                        }

                        {
                            Category_name == 'Interview' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Interviewer </h4>
                                </Col> 
                            </Row>
                            <div className="mb-4">
                                {additionalList.map((x, i) =>
                                    <div className="" > 
                                        {x.hide ?
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleadditionalChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                    {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                    {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }
                                        {additionalList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleadditionalAddClick}>Add Interviewer <AiIcons.AiOutlinePlusCircle color="white" /></button>}
                                    </div>
                                )
                            }
                            </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Personal Communication' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Recipient  </h4>
                                </Col>
                            </Row>
                                {additionalList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleadditionalChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                    {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                    {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {additionalList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleadditionalAddClick}>Add Recipient <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                            </> : ''
                        }
                        {
                            Category_name == 'Review' ? <> <Row>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="">Author of Review Item  </h4>
                                </Col>
                            </Row>
                                {additionalList.map((x, i) =>
                                    <div className="" >

                                        {x.hide ?
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleadditionalChange(e, i)} required />

                                                </Col>
                                                <Col lg={{ size: 5 }} xs={{ size: 4 }}>
                                                    <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                    {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                </Col>
                                            </Row> :
                                            <Row className="m_b_10 m-t-10" >
                                                <Col lg={{ size: 10 }} xs={{ size: 12 }} className="" id={i}>
                                                    <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                </Col>
                                                <Col lg={{ size: 2 }} xs={{ size: 4 }} className="p-0 m-auto ref_class_nav">
                                                    <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                    {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                </Col>
                                            </Row>
                                        }

                                        {additionalList.length - 1 === i && <p className="btn btn-info m-t-10" onClick={handleadditionalAddClick}>Add Review Item <AiIcons.AiOutlinePlusCircle color="white" /></p>}

                                    </div>
                                )}
                            </> : ''
                        }
                        {
                            Category_name == 'Book Chapter' ? <> <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-4">
                                    <h4 className="modal-sub-title">Book Chapter </h4>
                                    <input type='text' name="Title_of_chapter" className="form-control" placeholder="Title of Chapter" required />
                                </Col> 
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="modal-main-title">Book Contributors </h4>
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Book Author </h4>
                                </Col>
                            </Row>
                                <div className="mb-4">
                                    {additionalList.map((x, i) =>
                                        <div className="" >

                                            {x.hide ?
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input name="given" placeholder="Enter First Name" className="form-control" value={x.given} onChange={e => handleadditionalChange(e, i)} required />
                                                    </Col>
                                                    <Col lg={{ size: 5 }} xs={{ size: 5 }}>
                                                        <input className="ml10" name="family" placeholder="Enter Last Name" value={x.family} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size:2 }} className="ref_class_nav">
                                                        <MdIcons.MdFace color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                        {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                    </Col>
                                                </Row> :
                                                <Row className="m_b_10" >
                                                    <Col lg={{ size: 10 }} xs={{ size: 10 }} className="" id={i}>
                                                        <input type="text" name="literal" placeholder="Enter company name" value={x.literal} onChange={e => handleadditionalChange(e, i)} className="form-control" required />
                                                    </Col>
                                                    <Col lg={{ size: 2 }} xs={{ size: 2 }} className="ref_class_nav">
                                                        <MdIcons.MdBusiness color="black" size="30" name="hide" onClick={e => handleadditionalChange(e, i)} />
                                                        {additionalList.length !== 1 && <AiIcons.AiFillDelete color="red" size="30" onClick={() => handleadditionalRemoveClick(i)} />}
                                                    </Col>
                                                </Row>
                                            }

                                            {additionalList.length - 1 === i && <button type="button" className="btn btn-info" onClick={handleadditionalAddClick}>Add Book Author <AiIcons.AiOutlinePlusCircle color="white" /></button>}

                                        </div>
                                    )}
                                </div>
                            </> : ''
                        }
                        {
                            Category_name == 'Blog Post' ? <Row>
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapse(false)}>
                                                        <h4 className="">Blog Post</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapse(true)}>
                                                        <h4 className="">Blog Post</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_Post" className="form-control" placeholder="Title of Post" onChange={(e) =>handleChange(e)} required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" onChange={(e) =>handleChange(e)} required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='url' name="url" onChange={(e) =>handleChange(e)} className="form-control" placeholder="URL" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Accessed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed" onChange={(e) =>handleChange(e)} required />
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-4">
                                    <h4 className="modal-sub-title">Main Website</h4>
                                    <input type='text' name="Blog_Title" className="form-control" onChange={(e) =>handleChange(e)} placeholder="Blog Title" required />
                                </Col> 
                                
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" onChange={(e) =>handleChange(e)} placeholder="Annotation"></textarea> 
                                </Col> 
                                
                            </Row> : ''
                        }
                        {
                            Category_name == 'Book' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Book</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Book</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row> 
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_Book" className="form-control" placeholder="Title of Book" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Originally_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Originally Published"  />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Edition" className="form-control" placeholder="Edition"  />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Volume number" className="form-control" placeholder="Volume number"  />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher Place" className="form-control" placeholder="Publisher Place" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="ISBN" className="form-control" placeholder="ISBN"  />
                                                    </Col>
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? 
                                                            <label class="switch">
                                                                <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>  
                                                                <div className="d-flex align-items-center flex-wrap w-100">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion> 
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Book Chapter' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Book</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Book</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row> 
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                                        <h4 className="">Book</h4>
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_Book" className="form-control" placeholder="Title of Book" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Published" className="form-control" placeholder="Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher Place" className="form-control" placeholder="Publisher Place" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col> 
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex align-items-center flex-wrap w-100">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Book Review' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Review</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Review</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>  
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_Review" className="form-control" placeholder="Title of Book" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col> 
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                        <>
                                                            <div className="d-flex align-items-center flex-wrap w-100 mt-4">
                                                                <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                    <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                </Col>
                                                                <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                    <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                </Col>
                                                            </div>
                                                        </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>  
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="modal-main-title">Book Contributors </h4>
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }}>
                                    <h4 className="modal-sub-title">Original Book Author  </h4>
                                </Col>
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Book</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Book</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row> 
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_chapter" className="form-control" placeholder="Title of Book" required />
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion> 
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Conference Paper' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Paper/ Proceedings</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Paper/ Proceedings</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>  
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_paper" className="form-control" placeholder="Title of paper" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="title_of_con_process" className="form-control" placeholder="Title of Conference Proceedings" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher_place" className="form-control" placeholder="Publisher place" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="volume_number" className="form-control" placeholder="volume Number" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="issue_number" className="form-control" placeholder="Issue Number" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col>
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">IS THE BOOK ONLINE ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex align-items-center w-100 mt-4">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <div className="mb-4 w-100">
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                        <h4 className="modal-sub-title">Conference</h4>
                                    </Col>
                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                        <input type='text' name="name_of_event" className="form-control" placeholder="Name of Event" required />
                                    </Col>
                                    <div className="d-flex flex-wrap">
                                        <Col lg={{ size: 6 }} xs={{ size: 6 }}>
                                            <input type='text' name="location_of_event" className="form-control" placeholder="Location of Event" required />
                                        </Col>
                                        <Col lg={{ size: 6 }} xs={{ size: 6 }}>
                                            <input type='text' name="date_of_event" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} className="form-control" placeholder="Date of Event" required />
                                        </Col>
                                    </div>
                                </div>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Dictionary Entry' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Entry</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Entry</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>  
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_Entry" className="form-control" placeholder="Title of Entry" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-10">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col> 
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex align-items-center w-100 mt-3">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <div className="mb-4 w-100">
                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                        <h4 className="modal-sub-title">Dictionary </h4>
                                    </Col>
                                    <div className="d-flex flex-wrap">
                                        <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                            <input type='text' name="name_of_dictionary" className="form-control" placeholder="Name of Dictionary" required />
                                        </Col>
                                        <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                            <input type='text' name="date_published" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} className="form-control" placeholder="Date of Published" required />
                                        </Col>
                                        <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                            <input type='text' name="publisher" className="form-control" placeholder="publisher" required />
                                        </Col>
                                        <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                            <input type='text' name="publisher_place" className="form-control" placeholder="publisher place" required />
                                        </Col>
                                    </div>
                                </div>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'E-book' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">E-Book</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">E-Book</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>  
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_e-book" className="form-control" placeholder="Title of E-Book" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Publisher_place" className="form-control" placeholder="Publisher place" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="edition" className="form-control" placeholder="Edition" />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="volume_number" className="form-control" placeholder="volume Number" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-10">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='url' name="url" className="form-control" placeholder="URL" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                    </Col>
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex align-items-center w-100 mt-3">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Encyclopedia Entry' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Entry</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Entry</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>  
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_Entry" className="form-control" placeholder="Title of Entry" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col>
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex align-items-center w-100 mt-3">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion> 
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Encylopedia</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Encylopedia</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>   
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="name_of_encylopedia" className="form-control" placeholder="Name of Encylopedia" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="edition" className="form-control" placeholder="Edition" />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="volume_number" className="form-control" placeholder="Volume Number" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="date_published" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} className="form-control" placeholder="Date of Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="publisher" className="form-control" placeholder="publisher" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="publisher_place" className="form-control" placeholder="publisher place" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="database_name" className="form-control" placeholder="Database Name" required />
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion> 
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Film Movie' ? 
                            <Row className="m_b_10 "> 
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle   eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Film/Movie</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Film/Movie</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>    
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_film_movie" className="form-control" placeholder="Film/Movie" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Medium" className="form-control" placeholder="Medium" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Distributor" className="form-control" placeholder="distributor" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Distributor Location" className="form-control" placeholder="distributor_location" required />
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>  
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Image' ? 
                            <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Film/Movie</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Film/Movie</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>     
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title" className="form-control" placeholder="Title" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="container" className="form-control" placeholder="Container" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='url' name="url" className="form-control" placeholder="URL" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_accessed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed" required />
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Interview' ? <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Interview</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Interview</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>   
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_interview" className="form-control" placeholder="Title of Interview" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_of_interview" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date of Interview" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Publication_or_Container" className="form-control" placeholder="Publication or Container" required />
                                                    </Col>
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex flex-wrap w-100 mt-3">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Journal' ? <Row className="m_b_10 ">
                                <Accordion className="col-lg-12 mb-4" key="0" defaultActiveKey="0">
                                    <Card>
                                        <Card.Header > 
                                            <Accordion.Toggle eventKey="0"> 
                                                {colleapse_annotation ?  
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(false)}>
                                                        <h4 className="">Article</h4> 
                                                        <FaIcons.FaAngleDown size="30" />  
                                                        {/* <FaIcons.FaAngleUp onClick={() =>setColleapse(true)}/>   */}
                                                    </Col> 
                                                    :
                                                    <Col className="d-flex align-items-center p-0 card-header" onClick={() =>setColleapseAnnotation(true)}>
                                                        <h4 className="">Article</h4> 
                                                        {/* <FaIcons.FaAngleDown size="30" onClick={() =>setColleapse(false)} />   */}
                                                        <FaIcons.FaAngleUp/>  
                                                    </Col> 
                                                }
                                            </Accordion.Toggle>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                                <Row>  
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Title_of_article" className="form-control" placeholder="Title of Article" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="Date_Published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                                        <input type='text' name="Page Range" className="form-control" placeholder="Page Range" required />
                                                    </Col>
                                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                                        <input type='text' name="dol" className="form-control" placeholder="DOI"  />
                                                    </Col> 
                                                    <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                                        <h5 className="custom-toggled">Is the Book Online ? <label class="switch">
                                                            <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                                            <span class="slider round"></span>
                                                        </label></h5>
                                                    </Col>
                                                    {
                                                        check_online ?
                                                            <>
                                                                <div className="d-flex flex-wrap w-100 mt-3">
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                                                    </Col>
                                                                    <Col lg={{ size: 6 }} xs={{ size: 6 }} className="">
                                                                        <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                                                    </Col>
                                                                </div>
                                                            </> : ''
                                                    }
                                                </Row>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion> 
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="modal-sub-title">Journal </h4>
                                </Col>
                                <div className="mb-4 d-flex flex-wrap">
                                    <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                        <input type='text' name="name_of_journal" className="form-control" placeholder="Name of Journal" required />
                                    </Col>
                                    <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                        <input type='text' name="volume_number" className="form-control" placeholder="Volume Number" required />
                                    </Col>
                                    <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                        <input type='text' name="issue_number" className="form-control" placeholder="Issue Number" required />
                                    </Col>
                                    <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                        <input type='text' name="database_name" className="form-control" placeholder="Database Name"  />
                                    </Col>
                                </div>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Legal Bill' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Bill</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_bill" className="form-control" placeholder="Title of Bill" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Date_issued" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Issued" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="The_house" className="form-control" placeholder="The house the bill was heard in" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="session" className="form-control" placeholder="Session" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="bill_number" className="form-control" placeholder="Bill Number" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Publication </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                    <input type='text' name="Place_of_Publication" className="form-control" placeholder="Place of Publication" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                    <input type='text' name="issue_number" className="form-control" placeholder="Issue Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                    <input type='text' name="database_name" className="form-control" placeholder="Database Name" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Legal Case' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Case</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="case_name" className="form-control" placeholder="Case Name" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Date_of_decision" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date of Decision" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="judicial_authority" className="form-control" placeholder="Issuing/Judicial Authority" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Section_Report" className="form-control" placeholder="Section of Law Report" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="volume_number" className="form-control" placeholder="Volume Number" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Legislation' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Legislation</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_Legislation" className="form-control" placeholder="Title of Legislation" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date of Decision" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="section_number" className="form-control" placeholder="Section Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="reprint_number" className="form-control" placeholder="Reprint Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="volume_number" className="form-control" placeholder="Volume Number" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Magazine Article' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Article</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_article" className="form-control" placeholder="Title of Article" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="page_range" className="form-control" placeholder="Page Range" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Magazine </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Name_of_magazine" className="form-control" placeholder="Name of Magazine" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 4 }}>
                                    <input type='text' name="database_name" className="form-control" placeholder="Database Name"  />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Map' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Map</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_map" className="form-control" placeholder="Title of Map" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="collection_title" className="form-control" placeholder="Collection Title" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="item_number" className="form-control" placeholder="Item Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="scale_of_map" className="form-control" placeholder="Scale of Map" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Publisher </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }}>
                                    <input type='text' name="publisher_place" className="form-control" placeholder="Publisher Place" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'News Article' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Article</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_article" className="form-control" placeholder="Title of Article" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="page_range" className="form-control" placeholder="Page Range" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Newspaper </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="name_of_news_source" className="form-control" placeholder="Name of News Source" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Patent' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Patent</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_patent" className="form-control" placeholder="Title of Patent" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_issued" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Issued" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Number" className="form-control" placeholder="Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="issuing_authority" className="form-control" placeholder="Issuing Authority" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Personal Communication' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Communication</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_communication" className="form-control" placeholder="Title of Communication" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_communication" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date of Communication" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Medium" className="form-control" placeholder="Medium" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Regulation' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Regulation</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_regulation" className="form-control" placeholder="Title of Regulation" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Report' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Report</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_report" className="form-control" placeholder="Title of Report" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="page_range" className="form-control" placeholder="Page Range" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Publisher </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }}>
                                    <input type='text' name="publisher_place" className="form-control" placeholder="Publisher Place" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Review' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Review</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_Review" className="form-control" placeholder="Title of Review" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="page_range" className="form-control" placeholder="Page Range" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Review Container </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Name_of_container" className="form-control" placeholder="Name of Container"  />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }}>
                                    <input type='text' name="volume_number" className="form-control" placeholder="Volume Number" />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }}>
                                    <input type='text' name="issue_Number" className="form-control" placeholder="Issue Number" />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Song' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Song</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_song" className="form-control" placeholder="Title of Song" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_produced" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Produced" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Medium" className="form-control" placeholder="Medium" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Publisher_producer" className="form-control" placeholder="Publisher/Producer" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Publisher_place" className="form-control" placeholder="Publisher Place" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Speech' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Speech</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_speech" className="form-control" placeholder="Title of Speech" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Medium" className="form-control" placeholder="Medium" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Event </h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="namw_of_event" className="form-control" placeholder="Name Of Event" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }}>
                                    <input type='text' name="event_place" className="form-control" placeholder="Event Place" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Standard' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Standard</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_standard" className="form-control" placeholder="Title of Standard" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="item_number" className="form-control" placeholder="Item Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Publisher_place" className="form-control" placeholder="Publisher Place" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Thesis Dissertation' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Thesis</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_thesis" className="form-control" placeholder="Title of Thesis" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Episode Number" className="form-control" placeholder="Document Type" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="institution_name" className="form-control" placeholder="Academic Publisher or Institution Name" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Page_range" className="form-control" placeholder="Page Range" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'TV Radio' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Broadcast</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_broadcast" className="form-control" placeholder="Title of Broadcast" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_broadcast" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Broadcast" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="episode_number" className="form-control" placeholder="Episode Number" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="format" className="form-control" placeholder="Format" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher/Broadcaster/Network" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Video' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Video</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_video" className="form-control" placeholder="Title of Video" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="format" className="form-control" placeholder="Format" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="">
                                    <h5>IS THE BOOK ONLINE ? <label class="switch">
                                        <input type="checkbox" onChange={() => setcheck_online(!check_online)} />
                                        <span class="slider round"></span>
                                    </label></h5>
                                </Col>
                                {
                                    check_online ?
                                        <>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='URL' name="url" className="form-control" placeholder="URL" required />
                                            </Col>
                                            <Col lg={{ size: 6 }} xs={{ size: 12 }} className="">
                                                <input type='text' name="Date_Accessed_viewed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed/viewed" required />
                                            </Col>
                                        </> : ''
                                }

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                        {
                            Category_name == 'Website' ? <Row className="m_b_10 ">

                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Page/Article</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Title_of_article" className="form-control" placeholder="Title of Article or Page" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_published" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Published" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m-b-20">
                                    <input type='url' name="url" className="form-control" placeholder="URL" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="date_accessed" className="form-control" onBlur={(e) => (e.currentTarget.type = "text")} onFocus={(e) => (e.currentTarget.type = "date")} placeholder="Date Accessed" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref m_b_10">
                                    <h4 className="">Website</h4>
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="website_name" className="form-control" placeholder="website Name" required />
                                </Col>
                                <Col lg={{ size: 6 }} xs={{ size: 12 }} className="m_b_10">
                                    <input type='text' name="Publisher" className="form-control" placeholder="Publisher" required />
                                </Col>
                                <Col lg={{ size: 12 }} xs={{ size: 12 }} className="title_ref mb-3">
                                    <h4 className="modal-sub-title">Annotation</h4>
                                    <textarea name="Annotation" className="form-control" placeholder="Annotation"></textarea>
                                </Col> 
                            </Row> : ''
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose_ref}>
                            Close
                            </Button>
                        <Button variant="info" type="submit">
                            Save
                            </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    )
}

const mapStateToProps = (state) => {
    const { login, sidebar, project, Dashboard } = state;
    return { ...login, ...sidebar, ...project, ...Dashboard };
};
export default connect(mapStateToProps, null)(Reference);