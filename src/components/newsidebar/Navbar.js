import React, { useState, Component } from 'react';
import { useDispatch, connect } from "react-redux";
import * as AiIcons from 'react-icons/ai';
import * as MiIcons from 'react-icons/md';
import * as FaIcons from 'react-icons/fa';
import * as HiIcons from 'react-icons/hi';
import Apple from "../../images/apple.png";
import Google from "../../images/google.png";
import { Link } from 'react-router-dom';
import { Tooltip } from "reactstrap";
import './Navbar.scss';
import { IconContext } from 'react-icons';
import { Dropdown, Modal, Button, Form, Col } from 'react-bootstrap';
import { projectList, projectSave, projectDelete, projectEdit, referenceList, setLoading, projectCpoy, projectCitation, referenceAllGrt, sendReference } from "../../actions/user/index";
import FullPageLoader from "../../components/FullPageLoader/loder";
import * as types from '../../actions/constants';
import ReactHtmlParser from "react-html-parser";
import Csl from "../../images/ic_regular_citation_style_logo.png";


export class Navbar extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        var user = localStorage.getItem("user");
        var obj = JSON.parse(user);
        dispatch(projectList(obj.id));
    }
    render() {
        const data = { ...this.props, ...this.state };
        return (<Navbar1 {...data} />);
    }
}

function Navbar1(props) {
    const dispatch = useDispatch();
    const [sidebar, setSidebar] = useState(true);

    const showSidebar = () => { setSidebar(!sidebar); setNav_scroll(false); }

    const [tncTooltipOpen, setTncTooltipOpen] = useState(false);
    const tncToggle = () => setTncTooltipOpen(!tncTooltipOpen);

    const [pnpTooltipOpen, setPnpTooltipOpen] = useState(false);
    const pnpToggle = () => setPnpTooltipOpen(!pnpTooltipOpen);

    const [show, setShow] = useState(false);
    const [show_edit, setShow_edit] = useState(false);

    const handleClose = () => {

        setShow(false);
        setsearch_data('')
    }
    const handleClose_edit = () => {

        setShow_edit(false);
        setselect_show(false)
        setsearch_data('')
    }
    const handleShow = () => setShow(true);
    const handleShow_edit = () => setShow_edit(true);

    const [project_name, setproject_name] = useState("")
    const [citation_id, setcitation_id] = useState("")
    const [search_data, setsearch_data] = useState("")
    const onsubmitData = (e) => {
        e.preventDefault();
        var user_id = localStorage.getItem("user");
        var obj = JSON.parse(user_id);
        var id = obj.id.toString()
        var ProjectDataSave = { project_name: project_name, citation_id: citation_id, user_id: id }
        setsearch_data('')
        setproject_name('')
        var check_project_name = true
        props.project_list.map((project) => (
            project.name === project_name ? check_project_name = false : null
        ))
        if (check_project_name) {
            dispatch(setLoading(true))
            dispatch(projectSave(ProjectDataSave))
            handleClose();
        }
        else {
            setshow_project_issue(true)
            handleClose();
        }
    }

    const [name, setname] = useState('')
    const [p_id, setp_id] = useState('')
    const [activeClass, setactiveClass] = useState('')
    const updateProject = (ids, name, c_name, c_id) => {
        setname(name)
        setsearch_data(c_name)
        setcitation_id(c_id)
        setp_id(ids)
        handleShow_edit();
    }
    const onsubmitDataEdit = (e) => {
        e.preventDefault();
        var ca_id = citation_id.toString()
        var pa_id = p_id.toString()
        var check_name = true;
        props.project_list.map((project) => (
            project.name === name && project.id != p_id ? check_name = false : null
        ))
        if (check_name) {
            dispatch(setLoading(true))
            var ProjectDataEdit = { p_name: name, citation_id: ca_id, c_name: search_data, PId: p_id }
            if (props.project_ids === p_id) {
                dispatch(projectCitation(ProjectDataEdit))
            }
            else {
                dispatch(projectEdit(ProjectDataEdit))
            }
            handleClose_edit();
        }
        else {
            // alert('There is already a project with this name')
            setshow_project_issue(true)
            handleClose_edit();

        }
    }
    const [select_show, setselect_show] = useState(false);
    const Select_toggle = () => setselect_show(!select_show);
    const selectUpdate = (id, name) => {
        setsearch_data(name)
        setcitation_id(id)
        Select_toggle();
    }
    const selectUpdateEdit = (id, name) => {
        setsearch_data(name)
        setcitation_id(id)
        Select_toggle();
    }

    const ref_list = (id, name, citation_name, count) => {
        setactiveClass(id)
        dispatch(setLoading(true))
        const ref_data = { id: id.toString(), page: '1', per_page: '15', project_name: name, citation_name: citation_name, count: count };
        dispatch(referenceList(ref_data));
    }
    const duplicate = (pro_id) => {
        // console.log(pro_id)
        dispatch(setLoading(true))
        dispatch(projectCpoy(pro_id.toString()))
    }
    const [show_delete_project, setShow_delete_project] = useState(false);
    const [show_delete_project_id, setShow_delete_project_id] = useState('');
    const handleClose_delete_project = () => setShow_delete_project(false);
    const handleShow_delete_project = (id) => {
        setShow_delete_project_id(id)
        setShow_delete_project(true);
    }
    const deleteProject = () => {
        var id = show_delete_project_id.toString()
        dispatch(projectDelete(id))
        setShow_delete_project(false);
    }
    const [show_project_issue, setshow_project_issue] = useState(false);
    const [share_Data_project, setshare_Data_project] = useState('')
    const handleClose_project_issue = () => setshow_project_issue(false);

    const share = (id, count, citation_name) => {
        if (count != 0) {

            project_share_style_set('off')

            handleShow_project_email()
        }
    }
    const project_share_style_set = (text) => {
        if (props.critation_name.includes("APA") || props.critation_name.includes("Psychological") || props.critation_name.includes("JAAPA")) {

            const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {text-indent: -30px; padding-left: 25px;  margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + (
                props.referenceAllData && props.referenceAllData.map((shareData_get) => {
                    if (text === 'on') {
                        return ('<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>')
                    }
                    else {
                        return ('<p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>')
                    }
                }).join(''))
                + "</body></html>"
            setshare_Data_project(abc.replace(/,/gi, ''))
            console.log(abc.replace(/,/gi, ''));
            // handleShow_project_email()

        }
        else if (props.critation_name.includes("Modern Language Association")) {
            const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p { padding-left: 25px; text-indent: -30px; margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + (
                props.referenceAllData && props.referenceAllData.map((shareData_get) => {
                    if (text === 'on') {
                        return ('<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>')
                    }
                    else {
                        return ('<p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>')
                    }
                }).join(''))
                + "</body></html>"
            setshare_Data_project(abc.replace(/,/gi, ''))
            console.log('h2');
            // handleShow_project_email()
        }
        else if (props.critation_name.includes("Turabian") || props.critation_name.includes("Chicago") || props.critation_name.includes("Modern Humanities Research Association") ||
            props.critation_name.includes("De Montfort University – Harvard") ||
            props.critation_name.includes("Elsevier - Harvard (with titles") ||
            props.critation_name.includes("Elsevier - Harvard 2") ||
            props.critation_name.includes("Elsevier - Harvard (without titles) ") ||
            props.critation_name.includes("Emerald - Harvard") ||
            props.critation_name.includes("Griffith College - Harvard") ||
            props.critation_name.includes("Bournemouth University -Harvard") ||
            props.critation_name.includes("Cape Peninsula University of Technology - Harvard") ||
            props.critation_name.includes("Coventry University - Harvard") ||
            props.critation_name.includes("Edge Hill University - Harvard") ||
            props.critation_name.includes("Harvard Educational Review") ||
            props.critation_name.includes("European Archaeology - Harvard") ||
            props.critation_name.includes("Fachhochschule Salzburg - Harvard") ||
            props.critation_name.includes("Falmouth University - Harvard") ||
            props.critation_name.includes("Imperial College London - Harvard") ||
            props.critation_name.includes("King’s College London - Harvard") ||
            props.critation_name.includes("Leeds Beckett University - Harvard") ||
            props.critation_name.includes("Leeds Metropolitan University - Harvard") ||
            props.critation_name.includes("University of Limerick (Cite it Right) - Harvard") ||
            props.critation_name.includes("Manchester Business School - Harvard") ||
            props.critation_name.includes("Newcastle University - Harvard") ||
            props.critation_name.includes("Staffordshire University - Harvard") ||
            props.critation_name.includes("Stellenbosch University - Harvard") ||
            props.critation_name.includes("Swinburne University of Technology - Harvard") ||
            props.critation_name.includes("The University of Melbourne - Harvard") ||
            props.critation_name.includes("The University of Sheffield - School of East Asian Studies - Harvard") ||
            props.critation_name.includes("The University of Sheffield - Town and Regional Planning - Harvard") ||
            props.critation_name.includes("University of Abertay Dundee - Harvard") ||
            props.critation_name.includes("University of Bath - Harvard") ||
            props.critation_name.includes("University of Brighton School of Environment & Technology - Harvard") ||
            props.critation_name.includes("University of Kent - Harvard") ||
            props.critation_name.includes("University of Leeds - Harvard") ||
            props.critation_name.includes("University of Sunderland - Harvard") ||
            props.critation_name.includes("University of Technology Sydney - Harvard") ||
            props.critation_name.includes("University of the West of England (Bristol) - Harvard") ||
            props.critation_name.includes("University of the West of Scotland - Harvard") ||
            props.critation_name.includes("University of Westminster - Harvard") ||
            props.critation_name.includes("Institute of Physics - Harvard") ||
            props.critation_name.includes("La Trobe University - Harvard") ||
            props.critation_name.includes("Nottingham Trent University Library - Harvard") ||
            props.critation_name.includes("Oxford Centre for Mission Studies - Harvard") ||
            props.critation_name.includes("SAGE - Harvard") ||
            props.critation_name.includes("Taylor & Francis - Harvard V") ||
            props.critation_name.includes("Taylor & Francis - Harvard X") ||
            props.critation_name.includes("University of Bradford - Harvard") ||
            props.critation_name.includes("University of Lincoln - Harvard")) {
            const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p { padding-left: 30px; text-indent: -35px; margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + (
                props.referenceAllData && props.referenceAllData.map((shareData_get) => {
                    if (text === 'on') {
                        return ('<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p style="line-height: 1.5;">' + shareData_get.formatted_data + '</p><br>')
                    }
                    else {
                        return ('<p style="line-height: 1.5;">' + shareData_get.formatted_data + '</p><br>')
                    }
                }).join(''))
                + "</body></html>"
            setshare_Data_project(abc.replace(/,/gi, ''))
            console.log('h3');
            // handleShow_project_email()
        }
        else {
            const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + (
                props.referenceAllData && props.referenceAllData.map((shareData_get) => {
                    if (text === 'on') {
                        return ('<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p>' + shareData_get.formatted_data + '</p><br>')
                    }
                    else {
                        return ('<p>' + shareData_get.formatted_data + '</p><br>')
                    }
                }).join(''))
                + "</body></html>"
            setshare_Data_project(abc.replace(/,/gi, ''))
            console.log(abc.replace(/,/gi, ''));
            // handleShow_project_email()
        }
    }
    // console.log(share_Data_project)
    const [show_project_email, setShow_project_email] = useState(false);

    const handleClose_project_email = () => setShow_project_email(false);
    const handleShow_project_email = () => setShow_project_email(true);
    const [send_email, setsend_email] = useState('')
    // console.log(props)
    const Reference_Send = (e) => {
        e.preventDefault();
        // console.warn(send_email)
        dispatch(sendReference(send_email, share_Data_project))
        setShow_project_email(false)
        setsend_email('')

    }
    const [check_share_project, setCheck_share_project] = useState(false);
    const in_text_project = () => {
        if (check_share_project) {
            setCheck_share_project(false);
            project_share_style_set('off');
        }
        else {
            setCheck_share_project(true);
            project_share_style_set('on');
        }
    };
    const [nav_scroll, setNav_scroll] = useState(false);
    const a = window.innerWidth;
    if (a <= 991) {
        window.addEventListener('scroll', () => {
            let activeClass = 'normal';
            if (window.scrollY === 0) {
                activeClass = 'top';
            }
            setNav_scroll(true)
            setSidebar(false)
        });
    }
    return (
        <>
            { props.isLoading ? <FullPageLoader /> : <></>}
            <div className="sidebar easy-ref-sidebar">
                <IconContext.Provider value={{ color: '#fff' }}>
                    <div className='navbar_side align-items-center justify-content-between'>
                        <Link to='#' className='menu-bars'>
                            <FaIcons.FaBars onClick={showSidebar} />
                        </Link>
                        <div className="d-flex justify-content-end web-none mobile-app-image" lg={{ size: 4 }}>
                            <a className="header_border text-white" href="https://apps.apple.com/us/app/easy-referencing-citation/id1529091716" target="_blank">
                                <img className="image_crop p-0" src={Apple} />
                            </a >
                            {/* <AiIcons.AiFillApple size="20" />
                                <span className="hide_text">Available on Appstore</span> */}
                            <a className="header_border text-white ml-2" href="https://play.google.com/store/apps/details?id=com.easyref.project&hl=en_IN&gl=US" target="_blank">
                                <img className="image_crop p-0" src={Google} />
                                {/* <AiIcons.AiFillAndroid size="20" />
                                    <span className="hide_text">Available on Android</span> */}
                            </a>
                        </div>
                    </div>
                    <nav className={sidebar ? nav_scroll ? 'nav-menu ' : 'nav-menu active' : 'nav-menu'}>
                        <div className='nav-menu-items'>
                            <Link className="side_set mb" to='#' onClick={handleShow}>
                                <div className="project-new">
                                    <span className="text-white m-l-10"><AiIcons.AiOutlinePlus className="m-r-5" size="20" /></span>
                                    <span className="text-white font-size m-r-10">Start New Project</span>
                                </div>
                            </Link>
                            <Modal size="lg" show={show} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>New Project</Modal.Title>
                                </Modal.Header>
                                <form onSubmit={(values) => onsubmitData(values)}>
                                    <Modal.Body>
                                        <Form.Group>
                                            <Form.Label>Project Name</Form.Label>
                                            <Form.Control type="text" placeholder="project name" name="project_name" value={project_name} onChange={(e) => setproject_name(e.target.value)} onFocus={() => setselect_show(false)} required />

                                        </Form.Group>
                                        <Form.Label>Citation Style</Form.Label>
                                        <input className="form-control" type="text" placeholder="search..." value={search_data} onChange={(e) => setsearch_data(e.target.value)} onFocus={Select_toggle} required />
                                        <ul className={select_show ? 'visible select_tag m-0 p-0' : 'd-none'}>
                                            <h5 className="font-weight-bold bg-color m-t-10 p-2">Popular style</h5>
                                            {
                                                props.citation_popular && props.citation_popular.filter(person => person.title.includes(search_data)).map((popular) => (

                                                    <li className="p-2 hover_class_nav citation_font" onClick={() => selectUpdate(popular.id, popular.title)}>{popular.title}</li>

                                                ))
                                            }
                                            <span className="bg-color d-flex justify-content-between align-items-center">
                                                <p className="font-weight-bold  m-0  p-2">Regular style</p>
                                                <a href="https://citationstyles.org/" target="_blank"><img className="image_size_csl  m-r-10" src={Csl} /></a>
                                            </span>
                                            {
                                                props.citation_regular && props.citation_regular.filter(person => person.title.includes(search_data)).map((popular) => (

                                                    <li className="p-2 hover_class_nav citation_font" onClick={() => selectUpdate(popular.id, popular.title)}>{popular.title}</li>

                                                ))
                                            }
                                        </ul>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Close
                                    </Button>
                                        <Button variant="info" type="submit">
                                            Save
                                    </Button>
                                    </Modal.Footer>
                                </form>
                            </Modal>
                            <div className="overscroll">
                                {
                                    props.project_list && props.project_list.map((project) => (
                                        <div>
                                            {project ?
                                                <div className="nav-main-div" >
                                                    <div className="">
                                                        <Link className={props.project_ids === project.id ? 'side_menu_link online' : 'side_menu_link'} to='/live' onClick={() => ref_list(project.id, project.name, project.citation_name, project.reference_count)}>
                                                            <div className="text-white">
                                                                <span className="label">{project.name}</span>
                                                                <span className="count">{project.reference_count}</span>
                                                                <div className="clearfix"></div>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <Dropdown className="nav-main hide_dorpdown">
                                                        <Dropdown.Toggle id="dropdown-basic" className="p-0 m-t-5">
                                                            <MiIcons.MdMoreVert size="30" />
                                                        </Dropdown.Toggle>
                                                        <Dropdown.Menu>
                                                            <Dropdown.Item className="" onClick={() => updateProject(project.id, project.name, project.citation_name, project.citation_style_id)}><span><MiIcons.MdEdit size="30" color="black" /></span><span className="m-l-5 font_size" >Rename</span></Dropdown.Item>
                                                            <Dropdown.Item onClick={() => duplicate(project.id)}><span><HiIcons.HiOutlineDuplicate size="30" color="black" /></span><span className="m-l-5 font_size" >Duplicate</span></Dropdown.Item>
                                                            {props.project_ids === project.id ?
                                                                <Dropdown.Item onClick={() => share(project.id, project.reference_count, project.citation_name)}><span><FaIcons.FaShare size="25" color="black" /></span><span className="m-l-5 font_size" >Share</span></Dropdown.Item> : ''}
                                                            <Dropdown.Divider />
                                                            <Dropdown.Item className="error text-center" onClick={() => handleShow_delete_project(project.id)}><span><AiIcons.AiFillDelete size="30" color="red" /></span><span className="m-l-5 font_size" >Delete</span></Dropdown.Item>
                                                            {/* <Dropdown.Item className="error" onClick={() => deleteProject(project.id)}><span><AiIcons.AiFillDelete size="30" color="red" /></span><span className="m-l-5 font_size" >Delete</span></Dropdown.Item> */}
                                                        </Dropdown.Menu>
                                                    </Dropdown>
                                                </div>
                                                : null}
                                        </div>
                                    ))
                                }
                            </div>

                        </div>
                        <div className="p-absolute text-center col-md-12 p-0">
                            <div className="bottom_border">
                                <Link to='/privacy'>
                                    <span className="text-white text-center"><AiIcons.AiFillLock id="pnp" size="40" /><Tooltip isOpen={pnpTooltipOpen} toggle={pnpToggle} target="pnp" placement="top">Privacy & Policy</Tooltip></span>
                                </Link>
                                <Link className="" to='/term_condition'>
                                    <span className="text-white"><MiIcons.MdVerifiedUser id="tnc" size="40" /><Tooltip isOpen={tncTooltipOpen} toggle={tncToggle} target="tnc" placement="top">Terms & Conditions</Tooltip></span>
                                </Link>
                            </div>
                        </div>
                    </nav>
                    <Modal size="lg" show={show_edit} onHide={handleClose_edit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Edit Project</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <form  onSubmit={(values) => onsubmitDataEdit(values)}>
                                <Form.Group>
                                    <Form.Label>Project Name</Form.Label>
                                    <Form.Control type="text" placeholder="project name" name="project_name" value={name} onChange={(e) => setname(e.target.value)} onFocus={() => setselect_show(false)} required />

                                </Form.Group>
                                <Form.Label>Citation Style</Form.Label>
                                <input className="form-control" type="text" placeholder="search..." value={search_data} onChange={(e) => setsearch_data(e.target.value)} onFocus={Select_toggle} required />
                                <ul className={select_show ? 'visible select_tag m-0 p-0' : 'd-none'}>
                                    <h5 className="font-weight-bold bg-color m-t-10 p-2">Popular style</h5>
                                    {
                                        props.citation_popular && props.citation_popular.filter(person => person.title.includes(search_data)).map((popular) => (

                                            <li className="p-2 hover_class_nav citation_font" onClick={() => selectUpdateEdit(popular.id, popular.title)}>{popular.title}</li>

                                        ))
                                    }
                                    <span className="bg-color d-flex justify-content-between align-items-center">
                                        <p className="font-weight-bold  m-0  p-2">Regular style</p>
                                        <a href="https://citationstyles.org/" target="_blank"><img className="image_size_csl  m-r-10" src={Csl} /></a>
                                    </span>
                                    {
                                        props.citation_regular && props.citation_regular.filter(person => person.title.includes(search_data)).map((popular) => (

                                            <li className="p-2 hover_class_nav citation_font" onClick={() => selectUpdateEdit(popular.id, popular.title)}>{popular.title}</li>

                                        ))
                                    }
                                </ul>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose_edit}>
                                        Close
                                    </Button>
                                    <Button variant="info" type="submit">
                                        Save
                                    </Button>
                                </Modal.Footer>
                            </form>
                        </Modal.Body>
                    </Modal>
                </IconContext.Provider>
                <Modal show={show_delete_project} onHide={handleClose_delete_project}>
                    <Modal.Header closeButton>
                        <Modal.Title>Project Delete</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure, you want to remove this Project?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose_delete_project}>
                            No
                    </Button>
                        <Button variant="danger" onClick={() => deleteProject()}>
                            Yes
                    </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={show_project_issue} onHide={handleClose_project_issue}>
                    <Modal.Header closeButton>
                        <Modal.Title>Name Error</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>There is already a project with this name</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose_project_issue}>
                            Ok
                    </Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={show_project_email} onHide={handleClose_project_email} size="lg" aria-labelledby="contained-modal-title-vcenter">
                    <Modal.Header closeButton>
                        <Modal.Title>Share Reference</Modal.Title>
                    </Modal.Header>
                    <form onSubmit={(values) => Reference_Send(values)}>
                        <Modal.Body >
                            <Col lg={{ size: 12 }} xs={{ size: 12 }} className="m-b-10">
                                <h5><b>Include in-text Citation?</b> <label class="switch m-l-15" >
                                    <input type="checkbox" onChange={() => in_text_project()} />
                                    <span class="slider round"></span>
                                </label></h5>
                            </Col>
                            <Col xs={{ size: 12 }} lg={{ size: 12 }} className="share_m_height_set">

                                {ReactHtmlParser(share_Data_project)}
                            </Col>

                            <Col xs={{ size: 12 }} lg={{ size: 12 }} className="position-fix border_top">
                                <label className="font-weight-bold m-t-10">Email</label>
                                <input className="form-control" type="email" name="share_email" value={send_email} onChange={(e) => setsend_email(e.target.value)} placeholder="Email Id" required></input>
                            </Col>

                        </Modal.Body>
                        <Modal.Footer className="justify-content-center">
                            {/* <Button variant="secondary" onClick={handleClose_project_email}>
                                Close
                    </Button> */}
                            <Button variant="info" type="submit">
                                Send Email
                    </Button>
                        </Modal.Footer>
                    </form>
                </Modal>
            </div>
        </>
    );
}
const mapStateToProps = (state) => {
    const { login, sidebar, project, Dashboard } = state;
    return { login, ...sidebar, ...project, ...Dashboard };
};

export default connect(mapStateToProps, null)(Navbar)