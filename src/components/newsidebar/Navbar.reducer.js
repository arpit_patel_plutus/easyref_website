import { PROJECT, PROJECT_LIST, PROJECT_ERROR, CITATION_LIST,REFERENCEALLDATA } from '../../actions/constants';

export default (state = {}, action) => {
    switch (action.type) {
        case PROJECT:
            return { ...state, project: action };
        case PROJECT_LIST:
            return { ...state, project_list: action.project_list.data };
        case PROJECT_ERROR:
            return { ...state, project_error: action.response1 };
        case CITATION_LIST:
            return { ...state, citation_popular: action.citation_list.popular_style, citation_regular: action.regular_style.regular_style };
        case REFERENCEALLDATA:
            return { ...state, referenceAllData:action.referenceAllData };
        default:
            return state;
    }
}