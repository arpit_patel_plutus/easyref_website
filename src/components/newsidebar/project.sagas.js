import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECTSAVE,PROJECT_LIST,PROJECT_ERROR,SHOW_LODER } from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (ProjectDataSave) => axios.get(`${apiUrl}projects/new/?name=${ProjectDataSave.ProjectDataSave.project_name}&user_id=${ProjectDataSave.ProjectDataSave.user_id}&citation_style=${ProjectDataSave.ProjectDataSave.citation_id}`);
const getProjectList = (ids) => axios.get(`${apiUrl}users/projects/${ids}`);

function* getProjectSaveSaga(ProjectDataSave) {
    try 
    {
        const response = yield call(getUsersStats,ProjectDataSave);
        const response1 = JSON.parse(response.data.replace(/&quot;/gi,'"'));
        // console.warn(response1);
        
        if (response1.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            // var ids = obj.id.toString();
            
            const projectList = yield call(getProjectList,obj.id);
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
                yield put({ type: SHOW_LODER, isLoading: false });
            } else {
                yield put({ type: PROJECT_ERROR, response });
                yield put({ type: SHOW_LODER, isLoading: false });
            }
        } 
        else 
        {
            yield put({ type: PROJECT_ERROR, response1 });
        }
    }
    catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(PROJECTSAVE, getProjectSaveSaga)]);
}

export default watchUserAuthentication;