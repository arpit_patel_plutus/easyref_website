import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECT,PROJECT_LIST,PROJECT_ERROR,CITATION_LIST,SHOW_LODER,REFERENCELISTGET,PROJECTCOPY, PROJECTNAME,REFERENCEALLGET,REFERENCEALLDATA,REFERENCESEND } from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (project_id) => axios.get(`${apiUrl}users/projects/${project_id.project_id}`);
const getProjectList = (id) => axios.get(`${apiUrl}users/projects/${id}`);
const getcitation = () => axios.get(`${apiUrl}citation_styles/all`);
const getprojectCopy = (project_id) => axios.post(`${apiUrl}projects/duplicate/?project_id=${project_id}`);
const getReferences = (ref_data) => axios.get(`${apiUrl}references/?project_id=${ref_data.id}&page=${ref_data.page}&per_page=${ref_data.per_page}`);
const sendRefe = (ref_send) => {
    axios({
        method: 'post',
        url: apiUrl + 'users/send_references',
        headers: {}, 
        data: {
          email: ref_send.send_email,
          string: ref_send.share_Data_project // This is the body part
        }
      });
}

function* getProjectlistSaga(project_id) {
    try {
        yield put({ type: SHOW_LODER, isLoading: true});
        const response = yield call(getUsersStats,project_id);
        const citation_response = yield call(getcitation);
        yield put({ type: CITATION_LIST, citation_list:citation_response.data.data,regular_style:citation_response.data.data});
        
        // console.log(response)
        if (response.data.errorCode === 0) {
            yield put({ type: PROJECT_LIST, project_list:response.data});
            yield put({ type: PROJECTNAME, project_name:response.data.data[0].name,critation_name:response.data.data[0].citation_name});
            const ref_id = response.data.data[0].id.toString()
            const ref_data = {id:ref_id,page:'1',per_page:'15'};
            const ref_response = yield call(getReferences,ref_data);
            // console.warn('ref_data', ref_response.data.data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list:ref_response.data, next_page:ref_response.data.next_page, current_page: ref_response.data.current_page - 1 , project_ids : response.data.data[0].id});
                yield put({ type: SHOW_LODER, isLoading: false });
                const refData = {id:response.data.data[0].id.toString(),page:'1',per_page:response.data.data[0].reference_count.toString()};
                const Ref_all = yield call(getReferences,refData);
                yield put({type:REFERENCEALLDATA , referenceAllData:Ref_all.data.data});
            }
        } else {
            yield put({ type: SHOW_LODER, isLoading: false });
            yield put({ type: PROJECT_ERROR, response });
        }
    } catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* postProjectCopy (project)
{
    try 
    {
        const response = yield call(getprojectCopy,project.project_id);
        // console.warn(response)
        if (response.data.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            // var ids = obj.id.toString();
            
            const projectList = yield call(getProjectList,obj.id);
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            yield put({ type: SHOW_LODER, isLoading: false });

        }
        else
        {

        }
    } catch (error) {
        
    }
}

function* sendReferences (ref_send) {
    try {
        const response = yield call(sendRefe,ref_send);

            console.log(ref_send)
            console.log(response)
    } 
    catch (error) 
    {

        
    }
}
function* watchUserAuthentication() {
    yield all([takeLatest(PROJECT, getProjectlistSaga)]);
    yield all([takeLatest(PROJECTCOPY, postProjectCopy)]);
    yield all([takeLatest(REFERENCESEND, sendReferences)]);
}

export default watchUserAuthentication;