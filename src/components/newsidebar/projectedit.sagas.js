import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECT_LIST,PROJECT_ERROR,PROJECTEDIT,SHOW_LODER,PROJECTCITATION,PROJECTNAME,REFERENCELISTGET } from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (ProjectDataEdit) => axios.get(`${apiUrl}projects/edit/${ProjectDataEdit.ProjectDataEdit.PId}/?name=${ProjectDataEdit.ProjectDataEdit.p_name}&Pid=${ProjectDataEdit.ProjectDataEdit.PId}&citation_style=${ProjectDataEdit.ProjectDataEdit.citation_id}`);
const getProjectList = (ids) => axios.get(`${apiUrl}users/projects/${ids}`);
const getReferences = (ref_data) => axios.get(`${apiUrl}references/?project_id=${ref_data.id}&page=${ref_data.page}&per_page=${ref_data.per_page}`);

function* getProjectSaveSaga(ProjectDataEdit) {
    try 
    {
        const response = yield call(getUsersStats,ProjectDataEdit);
        const response1 = JSON.parse(response.data.replace(/&quot;/g,'"'));
        if (response1.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            // var ids = obj.id.toString();
            
            const projectList = yield call(getProjectList,obj.id);
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
                yield put({ type: SHOW_LODER, isLoading: false });
            } else {
                yield put({ type: PROJECT_ERROR, response });
                yield put({ type: SHOW_LODER, isLoading: false });
            }
        } 
        else 
        {
            // yield put({ type: PROJECT_ERROR, response });
        }
    }
    catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}
function* getProjectSave(ProjectDataEdit) {
    try 
    {
        const response = yield call(getUsersStats,ProjectDataEdit);
        const response1 = JSON.parse(response.data.replace(/&quot;/g,'"'));
        if (response1.errorCode === 0) 
        {
            yield put({ type: PROJECTNAME, project_name:ProjectDataEdit.ProjectDataEdit.p_name,critation_name:ProjectDataEdit.ProjectDataEdit.c_name});
            // var ids = obj.id.toString();
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            const projectList = yield call(getProjectList,obj.id);
            // console.log(projectList)
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
                // yield put({ type: SHOW_LODER, isLoading: false });
            } else {
                yield put({ type: PROJECT_ERROR, response });
                // yield put({ type: SHOW_LODER, isLoading: false });
            }
            const ref_data = {id:ProjectDataEdit.ProjectDataEdit.PId.toString(),page:'1',per_page:'15'};
            const ref_response = yield call(getReferences,ref_data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list:ref_response.data, next_page:ref_response.data.next_page, current_page: ref_response.data.current_page - 1 , project_ids : projectList.data.data[0].id});
                // console.warn(ref_response)
            }
            yield put({ type: SHOW_LODER, isLoading: false });
        } 
        else 
        {
            // yield put({ type: PROJECT_ERROR, response });
            // yield put({ type: SHOW_LODER, isLoading: false });
        }
    }
    catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(PROJECTEDIT, getProjectSaveSaga)]);
    yield all([takeLatest(PROJECTCITATION, getProjectSave)]);
}

export default watchUserAuthentication;