import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECTDELETE,PROJECT_LIST,PROJECT_ERROR } from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (id) => axios.get(`${apiUrl}projects/delete/${id.id}`);
const getProjectList = (ids) => axios.get(`${apiUrl}users/projects/${ids}`);

function* getProjectDeleteSaga(id,project_id) {
    try 
    {
        const response = yield call(getUsersStats,id);
        const response1 = JSON.parse(response.data.replace(/&quot;/g,'"'));
        if (response1.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            // var ids = obj.id.toString();
            
            const projectList = yield call(getProjectList,obj.id);
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            // yield put({ type: PROJECT_LIST, project_list:response.data});
        } 
        else 
        {
            // yield put({ type: PROJECT_ERROR, response });
        }
    }
    catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(PROJECTDELETE, getProjectDeleteSaga)]);
}

export default watchUserAuthentication;