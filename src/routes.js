import React from "react";
import { Redirect } from "react-router-dom";
const Live = React.lazy(() => import("./pages/live/live"));
const Login = React.lazy(() => import("./pages/login/login"));
const Management = React.lazy(() => import("./pages/management/management"));
const ResetPassword = React.lazy(() => import("./pages/reset-password/reset-password"));
const Users = React.lazy(() => import("./pages/users/users"));
const Privacy = React.lazy(() => import("./pages/privacy/Privacy"));
const TeamCondition = React.lazy(() => import("./pages/teamCondition/TeamCondition"));

export const routes = [
  { path: "/login", exact: true, component: Login },
  { path: "/reset-password", component: ResetPassword },
  { path: "/*", component: () => <Redirect to="/login" /> },
]

export const protectedroutes = [
  { path: "/live", component: Live },
  { path: "/users", component: Users },
  { path: "/management", component: Management },
  { path: "/privacy", component: Privacy },
  { path: "/term_condition", component: TeamCondition },
];