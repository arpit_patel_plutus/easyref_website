/*
 * @file: history.js
 * @description: History Configration
 * @date: 28.11.2019
 * @author: Pankaj Pandey
 * */

import { createBrowserHistory } from 'history';
/*********** History function **************/
export const history = createBrowserHistory();