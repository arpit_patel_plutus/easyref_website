/*
 * @file: index.js
 * @description: It contain user related action creators.
 * @author: Pankaj Pandey
 */
import * as TYPE from '../constants';
export const loginUserAction = (user, history) => {
    return {
        type: TYPE.USER_LOGIN,
        user,
        history
    }
};

export const resetPasswordAction = (user, history) => {
    return {
        type: TYPE.USER_RESET_PASSWORD,
        user,
        history
    }
};

export const getStatsAction = (user) => {
    return {
        type: TYPE.GET_STATS,
        user
    }
};

export const setStatsAction = (data) => {
    return {
        type: TYPE.SET_STATS,
        stats: data
    }
};

export const setLoading = (data) => {
    return {
        type: TYPE.SHOW_LODER,
        isLoading: data
    }
};
export const projectList = (project_id) => {
    return {
        type: TYPE.PROJECT,
        project_id
    }
};
export const projectCpoy = (project_id) => {
    return {
        type: TYPE.PROJECTCOPY,
        project_id
    }
};
export const projectSave = (ProjectDataSave) => {
    return {
        type: TYPE.PROJECTSAVE,
        ProjectDataSave
    }
};
export const projectDelete = (id) => {
    return {
        type: TYPE.PROJECTDELETE,
        id
    }
};
export const projectEdit = (ProjectDataEdit) => {
    return {
        type: TYPE.PROJECTEDIT,
        ProjectDataEdit
    }
};
export const projectCitation = (ProjectDataEdit) => {
    return {
        type: TYPE.PROJECTCITATION,
        ProjectDataEdit
    }
};
export const referenceList = (ref_data) => {
    return {
        type: TYPE.REFERENCELIST,
        ref_data
    }
};
export const referenceDelete = (id,project_id) => {
    return {
        type: TYPE.REFERENCEDELETE,
        id,project_id
    }
};
export const referenceMove = (project_id,reference_id,old_pro_id) => {
    return {
        type: TYPE.REFERENCEMOVE,
        project_id,reference_id,old_pro_id
    }
};
export const referenceMoveMultiple = (ids,project_id,move_pro_id) => {
    return {
        type: TYPE.REFERENCEMOVEMULTIPLE,
        ids,project_id,move_pro_id
    }
};

export const referenceDeleteMultiple = (ids,old_project_id) => {
    return {
        type: TYPE.REFERENCEDELETEMULTIPLE,
        ids,old_project_id
    }
};
export const referenceCopy = (ids,project_id,copy_project_id) => {
    return {
        type: TYPE.REFERENCECOPY,
        ids,project_id,copy_project_id
    }
};
export const referenceAllGrt = (ref_data) => {
    return {
        type: TYPE.REFERENCEALLGET,
        ref_data
    }
};
export const sendReference = (send_email,share_Data_project) => {
    return {
        type: TYPE.REFERENCESEND,
        send_email,share_Data_project
    }
};
export const createReference = (project_id,type,data) => {
    return {
        type: TYPE.REFERENCECREATE,
        project_id,type,data
    }
};