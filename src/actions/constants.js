/*
 * @file: constants.js
 * @description: It contain action types related action.
 * @author: Pankaj Pandey
 */

/*********** USER ***********/
export const USER_LOGIN = 'USER_LOGIN';
export const USER_RESET_PASSWORD = 'USER_RESET_PASSWORD';
export const USER_RESET_ERROR = 'USER_RESET_ERROR';
export const USER_LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const USER_LOGIN_ERROR = 'LOGIN_USER_ERROR';
export const LOG_OUT = 'LOG_OUT';
export const USER_PASSWORD = 'USER_PASSWORD';


/************ LOADING *************/
export const IS_LOADING = 'IS_LOADING';
export const SHOW_LODER = 'SHOW_LODER';
export const HIDE_LODER = 'HIDE_LODER';



/************ STATS *************/
export const GET_STATS = 'GET_STATS';
export const SET_STATS = 'SET_STATS';

/************ PROJECT *************/
export const PROJECT = 'PROJECT';
export const PROJECTCOPY = 'PROJECTCOPY';
export const PROJECTSAVE = 'PROJECTSAVE';
export const PROJECT_LIST = 'PROJECT_LIST';
export const PROJECT_ERROR = 'PROJECT_ERROR';
export const PROJECTDELETE = 'PROJECTDELETE';
export const PROJECTEDIT = 'PROJECTEDIT';
export const CITATION_LIST = 'CITATION_LIST';


export const REFERENCELIST = 'REFERENCELIST';
export const REFERENCELISTGET = 'REFERENCELISTGET';
export const REFERENCEDELETE = 'REFERENCEDELETE';
export const REFERENCEDELETEMULTIPLE = 'REFERENCEDELETEMULTIPLE';
export const REFERENCEMOVE = 'REFERENCEMOVE';
export const REFERENCEMOVEMULTIPLE = 'REFERENCEMOVEMULTIPLE';
export const REFERENCECOPY = 'REFERENCECOPY';
export const REFERENCESEND = 'REFERENCESEND';
export const PROJECTNAME = 'PROJECTNAME';
export const PROJECTCITATION = 'PROJECTCITATION';
export const REFERENCEALLGET = 'REFERENCEALLGET';
export const REFERENCEALLDATA = 'REFERENCEALLDATA';



export const REFERENCECREATE = 'REFERENCECREATE';


