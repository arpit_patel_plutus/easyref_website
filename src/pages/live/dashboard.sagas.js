import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECT_ERROR,REFERENCELIST,REFERENCELISTGET ,SHOW_LODER,PROJECTNAME,REFERENCEALLDATA} from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (ref_data) => axios.get(`${apiUrl}references/?project_id=${ref_data.ref_data.id}&page=${ref_data.ref_data.page}&per_page=${ref_data.ref_data.per_page}`);
const getReferences = (ref_data) => axios.get(`${apiUrl}references/?project_id=${ref_data.id}&page=${ref_data.page}&per_page=${ref_data.per_page}`);

function* getProjectlistSaga(ref_data) {
    try {
        // console.warn('ref',ref_data)
        const response = yield call(getUsersStats,ref_data);
        if (response.data.errorCode === 0) {
            yield put({ type: PROJECTNAME, project_name:ref_data.ref_data.project_name,critation_name:ref_data.ref_data.citation_name});
            yield put({ type: REFERENCELISTGET, ref_list:response.data, next_page:response.data.next_page, current_page: response.data.current_page  - 1 , project_ids : Number(ref_data.ref_data.id)});

            yield put({ type: SHOW_LODER, isLoading: false });
            const refData = {id:ref_data.ref_data.id.toString(),page:'1',per_page:ref_data.ref_data.count.toString()};
            const Ref_all = yield call(getReferences,refData);
            yield put({type:REFERENCEALLDATA , referenceAllData:Ref_all.data.data});
        } else {
            yield put({ type: SHOW_LODER, isLoading: false });
            yield put({ type: PROJECT_ERROR, response });
        }
    } catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(REFERENCELIST, getProjectlistSaga)]);
}

export default watchUserAuthentication;