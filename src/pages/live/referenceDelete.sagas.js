import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import {PROJECT_ERROR,REFERENCELISTGET ,SHOW_LODER, REFERENCEDELETE,PROJECT_LIST,REFERENCEDELETEMULTIPLE} from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (id) => axios.get(`${apiUrl}references/delete/${id.id}`);
const getReferenceDelete = (ref_del) => axios.post(`${apiUrl}references/delete/?ids=${ref_del}`);
const getProjectList = (ids) => axios.get(`${apiUrl}users/projects/${ids}`);
const getReferences = (ref_data) => axios.get(`${apiUrl}references/?project_id=${ref_data.id}&page=${ref_data.page}&per_page=${ref_data.per_page}`);

function* getProjectlistSaga(id) {
    try {
        const response = yield call(getUsersStats,id);
        if (response.data.errorCode === 0) {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            const projectList = yield call(getProjectList,obj.id);
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            const ref_data = {id:id.project_id,page:'1',per_page:'15'};
            const ref_response = yield call(getReferences,ref_data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list:ref_response.data ,next_page:ref_response.data.next_page, current_page: ref_response.data.current_page  - 1 , project_ids : Number(id.project_id)});
            
            }
            yield put({ type: SHOW_LODER, isLoading: false });
        } 
        else {
            yield put({ type: SHOW_LODER, isLoading: false });
            // yield put({ type: PROJECT_ERROR, response });
        }
    } catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}
function* deleteMultipleSaga (ref_delete) {
    try{
        console.warn(ref_delete.ids)
        const response = yield call(getReferenceDelete,ref_delete.ids);
        if (response.data.errorCode === 0) {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            const projectList = yield call(getProjectList,obj.id);
            if (projectList.data.errorCode === 0) {
                
                yield put({ type: PROJECT_LIST, project_list:projectList.data});
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            const ref_data = {id:ref_delete.old_project_id,page:'1',per_page:'15'};
            const ref_response = yield call(getReferences,ref_data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list:ref_response.data ,next_page:ref_response.data.next_page, current_page: ref_response.data.current_page  - 1 , project_ids : Number(ref_delete.old_project_id)});
            
            }
            yield put({ type: SHOW_LODER, isLoading: false });
        } 
        else {
            // yield put({ type: PROJECT_ERROR, response });
        }
    }
    catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }

}

function* watchUserAuthentication() {
    yield all([takeLatest(REFERENCEDELETE, getProjectlistSaga)]);
    yield all([takeLatest(REFERENCEDELETEMULTIPLE, deleteMultipleSaga)]);
}

export default watchUserAuthentication;