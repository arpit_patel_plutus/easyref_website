import React, { Component, useState, useEffect } from "react";
import { useDispatch,connect } from "react-redux";
import { Col, Row, Label, Input } from 'reactstrap';
import { referenceDelete, referenceMove, referenceList, setLoading, referenceDeleteMultiple, referenceMoveMultiple, referenceCopy, projectEdit, projectCitation, sendReference } from "../../actions/user/index";
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as HiIcons from 'react-icons/hi';
import * as MdIcons from 'react-icons/md';
import "./live.scss";
import ReactHtmlParser from "react-html-parser";
import * as MiIcons from'react-icons/md';
import copy from "copy-to-clipboard";
import FullPageLoader from "../../components/FullPageLoader/loder";
import Reference from "../../components/reference/Reference";
import { Dropdown, Modal, Button, OverlayTrigger, Tooltip, Accordion, Card, Toast } from 'react-bootstrap';
import Csl from "../../images/ic_regular_citation_style_logo.png";


export class Live extends Component {

    render() {

        const data = { ...this.props, ...this.state };
        return (<Dashboard {...data} />);

    }
}

const Dashboard = (props) => {
    const { dispatch } = useDispatch();
    const [show_edit, setShow_edit] = useState(false);
    const [ref_model, setref_model] = useState(false);
    const handleClose_edit = () => setShow_edit(false);
    const handleShow_edit = () => {

        setref_model(!ref_model)

    }
    const [search_data, setsearch_data] = useState("")
    const copyToClipboard = (text) => {
        var htmlObject = document.createElement('div');
        htmlObject.innerHTML = text;
        copy(htmlObject.innerText);
        setAlert_msg('Reference has been copied.')
        setShow_alert(true);
    }
    const copyToClipboardText = (text, formatted_data) => {
        var htmlObject = document.createElement('div');
        htmlObject.innerHTML = text + "\n" + formatted_data;
        copy(htmlObject.innerText);
        setAlert_msg('Reference has been copied.')
        setShow_alert(true);
    }

    const [show, setShow] = useState(false);
    const [ref_id, setref_id] = useState('');
    const [old_project_id, setold_project_id] = useState('');
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const move_ref = (id, old_pro_id) => {
        setref_id(id)
        setold_project_id(old_pro_id)
        handleShow()
    }
    const move_reference = (ids) => {

        handleClose()
        dispatch(setLoading(true))
        const project_id = ids.toString();
        const reference_id = ref_id.toString();
        const old_pro_id = old_project_id.toString()
        dispatch(referenceMove(project_id, reference_id, old_pro_id))
    }
    const nextPage = (page) => {
        const page_no = page + 2
        const id = props.reference_list[0].project_id
        dispatch(setLoading(true))
        const ref_data = { id: id.toString(), page: page_no.toString(), per_page: '15', project_name: props.project_name, citation_name: props.critation_name };
        dispatch(referenceList(ref_data));
    }
    const PreviusPage = (page) => {
        const page_no = page
        const id = props.reference_list[0].project_id
        dispatch(setLoading(true))
        const ref_data = { id: id.toString(), page: page_no.toString(), per_page: '15', project_name: props.project_name, citation_name: props.critation_name };
        dispatch(referenceList(ref_data));
    }
    const [isCheckAll, setIsCheckAll] = useState(false);
    const [isCheck, setIsCheck] = useState([])

    const handleSelectAll = e => {
        setIsCheckAll(!isCheckAll);
        setIsCheck(props.reference_list.map(li => li.id));
        if (isCheckAll) {
            setIsCheck([]);
        }
    };
    const handleClick = e => {

        const { id, checked } = e.target;

        setIsCheck([...isCheck, Number(id)]);
        if (!checked) {
            setIsCheckAll(false);
            setIsCheck(isCheck.filter(item => item !== Number(id)));
        }
    };
    const delete_ref_multiple = () => {
        const ids = isCheck.toString();
        // console.warn(ids)
        if (ids != '') {
            dispatch(setLoading(true))
            dispatch(referenceDeleteMultiple(ids, props.project_ids.toString()))
            setShow_delete_multiple(false)
            

                setAlert_msg('Reference Deleted sucessfully')

                setShow_alert(true);
            
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }

    }
    const [ref_mul_move, setref_mul_move] = useState(false)
    const handleCloseMoveRef = () => setref_mul_move(false)
    const handleshowMoveRef = () => {
        const ids = isCheck.toString();
        if (ids != '') {
            setref_mul_move(true)
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }
    }

    const move_ref_multiple = (move_project_id) => {
        handleCloseMoveRef()
        const ids = isCheck.toString();
        if (ids != '') {
            dispatch(setLoading(true))
            dispatch(referenceMoveMultiple(ids, props.project_ids.toString(), move_project_id.toString()))
            
                setAlert_msg('Reference Move sucessfully')

                setShow_alert(true);
            
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }
    }
    const [ref_mul_copy, setref_mul_copy] = useState(false)
    const [ref_copy_id, setref_copy_id] = useState('')
    const handleCloseCopyRef = () => setref_mul_copy(false)
    const handleShowCopyRef = () => setref_mul_copy(true)
    const copy_ref = (ref_id, project_id) => {
        const ids = isCheck.toString();
        if (ref_id || ids) {
            if (ref_id) {
                setref_copy_id(ref_id.toString())
            }
            setref_mul_copy(true)
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }
    }
    const copy_ref_multiple = (copy_project_id, project_name) => {
        const ids = isCheck.toString();
        dispatch(setLoading(true))
        if (ref_copy_id) {
            dispatch(referenceCopy(ref_copy_id, props.project_ids.toString(), copy_project_id.toString()))
        }
        else {
            dispatch(referenceCopy(ids, props.project_ids.toString(), copy_project_id.toString()))
        }
        handleCloseCopyRef()
        
            setAlert_msg('Reference has been copied ' + project_name + ' project successfully')
            setShow_alert(true);
        
    }
    useEffect(() => {
        // Update the document title using the browser API
        setIsCheckAll(false);
        setIsCheck([]);
    }, [props.reference_list]);
    const [show_delete_multiple, setShow_delete_multiple] = useState(false);

    const handleClose_delete_multiple = () => setShow_delete_multiple(false);
    const handleShow_delete_multiple = () => {
        const ids = isCheck.toString();
        if (ids != '') {
            setShow_delete_multiple(true);
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }
    }
    const [show_delete_single, setShow_delete_single] = useState(false);
    const [show_delete_single_id, setShow_delete_single_id] = useState('');
    const [show_delete_single_project_id, setShow_delete_single_project_id] = useState('');

    const handleClose_delete_single = () => setShow_delete_single(false);
    const handleShow_delete_single = (dele_id, dele_project_id) => {
        setShow_delete_single_id(dele_id)
        setShow_delete_single_project_id(dele_project_id)
        setShow_delete_single(true);
    }
    const ref_delete = () => {
        dispatch(setLoading(true))
        dispatch(referenceDelete(show_delete_single_id, show_delete_single_project_id))
        setShow_delete_single(false);
        
            setAlert_msg('Reference Deleted sucessfully')
            setShow_alert(true);
        

    }

    const [share_Data, setshare_Data] = useState('')

    const share = (text) => {
        
        const ids = isCheck.toString();
        if (ids != '') {
            if (props.critation_name.includes("APA") || props.critation_name.includes("Psychological") || props.critation_name.includes("JAAPA")) {

                const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {text-indent: -30px; padding-left: 25px;  margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + isCheck.map((check_id) =>
                    props.reference_list && props.reference_list.map((shareData_get) => {
                        if (text === 'on') {
                            return (shareData_get.id == check_id ? '<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                        else {
                            return (shareData_get.id == check_id ? '<p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                    }).join('')
                ) + "</body></html>"
                setshare_Data(abc.replace(/,/gi, ''))
                handleShow_share_email()
                console.log(abc.replace(/,/gi, ''));
            }
            else if (props.critation_name.includes("Modern Language Association")) {
                const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p { padding-left: 25px; text-indent: -30px; margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + isCheck.map((check_id) =>
                    props.reference_list && props.reference_list.map((shareData_get) => {
                        if (text === 'on') {
                            return (shareData_get.id == check_id ? '<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                        else {
                            return (shareData_get.id == check_id ? '<p style="line-height: 2;">' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                    }).join('')
                ) + "</body></html>"
                setshare_Data(abc.replace(/,/gi, ''))
                console.log('h2');
                handleShow_share_email()
            }
            else if (props.critation_name.includes("Turabian") || props.critation_name.includes("Chicago") || props.critation_name.includes("Modern Humanities Research Association") ||
                props.critation_name.includes("De Montfort University – Harvard") ||
                props.critation_name.includes("Elsevier - Harvard (with titles") ||
                props.critation_name.includes("Elsevier - Harvard 2") ||
                props.critation_name.includes("Elsevier - Harvard (without titles) ") ||
                props.critation_name.includes("Emerald - Harvard") ||
                props.critation_name.includes("Griffith College - Harvard") ||
                props.critation_name.includes("Bournemouth University -Harvard") ||
                props.critation_name.includes("Cape Peninsula University of Technology - Harvard") ||
                props.critation_name.includes("Coventry University - Harvard") ||
                props.critation_name.includes("Edge Hill University - Harvard") ||
                props.critation_name.includes("Harvard Educational Review") ||
                props.critation_name.includes("European Archaeology - Harvard") ||
                props.critation_name.includes("Fachhochschule Salzburg - Harvard") ||
                props.critation_name.includes("Falmouth University - Harvard") ||
                props.critation_name.includes("Imperial College London - Harvard") ||
                props.critation_name.includes("King’s College London - Harvard") ||
                props.critation_name.includes("Leeds Beckett University - Harvard") ||
                props.critation_name.includes("Leeds Metropolitan University - Harvard") ||
                props.critation_name.includes("University of Limerick (Cite it Right) - Harvard") ||
                props.critation_name.includes("Manchester Business School - Harvard") ||
                props.critation_name.includes("Newcastle University - Harvard") ||
                props.critation_name.includes("Staffordshire University - Harvard") ||
                props.critation_name.includes("Stellenbosch University - Harvard") ||
                props.critation_name.includes("Swinburne University of Technology - Harvard") ||
                props.critation_name.includes("The University of Melbourne - Harvard") ||
                props.critation_name.includes("The University of Sheffield - School of East Asian Studies - Harvard") ||
                props.critation_name.includes("The University of Sheffield - Town and Regional Planning - Harvard") ||
                props.critation_name.includes("University of Abertay Dundee - Harvard") ||
                props.critation_name.includes("University of Bath - Harvard") ||
                props.critation_name.includes("University of Brighton School of Environment & Technology - Harvard") ||
                props.critation_name.includes("University of Kent - Harvard") ||
                props.critation_name.includes("University of Leeds - Harvard") ||
                props.critation_name.includes("University of Sunderland - Harvard") ||
                props.critation_name.includes("University of Technology Sydney - Harvard") ||
                props.critation_name.includes("University of the West of England (Bristol) - Harvard") ||
                props.critation_name.includes("University of the West of Scotland - Harvard") ||
                props.critation_name.includes("University of Westminster - Harvard") ||
                props.critation_name.includes("Institute of Physics - Harvard") ||
                props.critation_name.includes("La Trobe University - Harvard") ||
                props.critation_name.includes("Nottingham Trent University Library - Harvard") ||
                props.critation_name.includes("Oxford Centre for Mission Studies - Harvard") ||
                props.critation_name.includes("SAGE - Harvard") ||
                props.critation_name.includes("Taylor & Francis - Harvard V") ||
                props.critation_name.includes("Taylor & Francis - Harvard X") ||
                props.critation_name.includes("University of Bradford - Harvard") ||
                props.critation_name.includes("University of Lincoln - Harvard")) {
                const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p { padding-left: 30px; text-indent: -35px; margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + isCheck.map((check_id) =>
                    props.reference_list && props.reference_list.map((shareData_get) => {
                        if (text === 'on') {
                            return (shareData_get.id == check_id ? '<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p style="line-height: 1.5;">' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                        else {
                            return (shareData_get.id == check_id ? '<p style="line-height: 1.5;">' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                    }).join('')
                ) + "</body></html>"
                setshare_Data(abc.replace(/,/gi, ''))
                console.log('h3');
                handleShow_share_email()
            }
            else {
                const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + isCheck.map((check_id) =>
                    props.reference_list && props.reference_list.map((shareData_get) => {
                        if (text === 'on') {
                            return (shareData_get.id == check_id ? '<b>In-Text Citation</b><p>' + shareData_get.intext_citation_data + '</p><br><p>' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                        else {
                            return (shareData_get.id == check_id ? '<p>' + shareData_get.formatted_data + '</p><br>' : "")
                        }
                    }).join('')
                ) + "</body></html>"
                setshare_Data(abc.replace(/,/gi, ''))

                handleShow_share_email()
            }
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }

    }
    const limit = 40
    const length_check = props.critation_name ? props.critation_name.length : ""
    const [show_criti, setShow_criti] = useState(false);

    const handleClose_criti = () => setShow_criti(false);
    const handleShow_criti = () => setShow_criti(true);
    const [search_criti, setsearch_criti] = useState("")
    const [citation_id_criti, setcitation_id_criti] = useState("")
    const [select_criti, setselect_criti] = useState(false);
    const Select_toggle = () => setselect_criti(!select_criti);
    const selectCritation = (id, name) => {
        setsearch_criti(name)
        setcitation_id_criti(id)
        Select_toggle();
        dispatch(setLoading(true))
        var ProjectDataEdit = { p_name: props.project_name, citation_id: id, c_name: name, PId: props.project_ids }
        dispatch(projectCitation(ProjectDataEdit))
        handleClose_criti()
        setsearch_criti('')
        // console.log(props.project_name);
    }
    const [show_share_email, setShow_share_email] = useState(false);

    const handleClose_share_email = () => setShow_share_email(false);
    const handleShow_share_email = () => setShow_share_email(true);
    const [send_email_project, setsend_email_project] = useState('')
    const send_email = (e) => {
        e.preventDefault();
        dispatch(sendReference(send_email_project, share_Data))
        setShow_share_email(false);
        setsend_email_project('')
        setIsCheckAll(false);
        setIsCheck([]);
        setAlert_msg('Email send Successfully')
            setShow_alert(true);
        // console.warn()
    }
    const [check_share, setCheck_share] = useState(false);
    const in_text = () => {
        if (check_share) {
            setCheck_share(false);
            share('off');
            
        }
        else {
            setCheck_share(true);
            share('on');
            
        }
        
    };
    const [show_alert, setShow_alert] = useState(false);
    const [alert_msg, setAlert_msg] = useState("")
    const [alert_delay, setAlert_delay] = useState(3000)

    const copy_references = () => {

        const ids = isCheck.toString();
        if (ids != '') {
            const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + isCheck.map((check_id) =>
                props.reference_list && props.reference_list.map((shareData_get) => {
                    return (shareData_get.id == check_id ? shareData_get.intext_citation_data + '\n' + shareData_get.formatted_data + '\n' : "")

                }).join('')
            )
            var aa = abc.replace(/,/gi, '');
            var ac = aa.replace("<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>", '');
            copy(ac)
            setAlert_msg('References successfully copied to clipboard')
            setShow_alert(true);

        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }
    }
    const downloadTxtFile = () => {
        const ids = isCheck.toString();
        if (ids != '') {
            const abc = "<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>" + isCheck.map((check_id) =>
                props.reference_list && props.reference_list.map((shareData_get) => {
                    return (shareData_get.id == check_id ? shareData_get.intext_citation_data + '\n' + shareData_get.formatted_data + '\n' : "")

                }).join('')
            )
            var aa = abc.replace(/,/gi, '');
            var ac = aa.replace("<html><head><style type='text/css'>body { font: 12pt 'MuseoSlab-500'; color: #555555;}i { color: #555555; }p {margin-block-start: 0px; margin-block-end: 0px; margin-inline-start: 0px; margin-inline-end: 0px;}</style></head><body>", '');
            const element = document.createElement("a");
            const file = new Blob([ac], { type: 'text/plain' });
            element.href = URL.createObjectURL(file);
            element.download = "Reference.txt";
            document.body.appendChild(element); // Required for this to work in FireFox
            element.click();

            setAlert_msg('References Download Successfully')
            setShow_alert(true);
        }
        else {
            setAlert_msg('No References are selected')
            setShow_alert(true);
        }
    }
    return (
        <>
            { props.isLoading ? <FullPageLoader /> : <></>}
            
            <div className="page-warp card easy-ref-wrapper">
                <div className="text-justify text-set ">
                {
                    props.isLoading == false ? <div
                    aria-live="polite"
                    aria-atomic="true"
                    style={{
                        position: 'relative',
                        zIndex: 2
                    }}
                >
                    <div className="ml_48"
                        style={{
                            position: 'fixed',
                            top: 0,
                            // right: 0,

                        }}
                    >
                        <Toast onClose={() => setShow_alert(false)} show={show_alert} delay={3000} autohide>
                            {/* <Toast.Header>
                                <img
                                    src="holder.js/20x20?text=%20"
                                    className="rounded mr-2"
                                    alt=""
                                />
                                <strong className="mr-auto">Success</strong>
                            </Toast.Header> */}
                            <Toast.Body className="alert_color">{alert_msg}</Toast.Body>
                        </Toast>
                    </div>
                </div> : ''
                }
                    
                    <Row className="m-0">
                        <Col lg={{ size: 4 }} xs={{ size: 12 }} className="easy-ref-titles">
                            <h5 className="easy-ref-title-name">{props.project_name}</h5>
                        </Col>
                        <OverlayTrigger
                            key='bottom'
                            placement='bottom'
                            overlay={
                                <Tooltip id={`tooltip-bottom`}>
                                    {props.critation_name ? props.critation_name : ''}
                                </Tooltip>
                            }
                        >
                            <Col lg={{ size: 4 }} xs={{ size: 12 }} className="critation_hover easy-ref-titles">
                                <h5 className="easy-ref-title-name" onClick={() => handleShow_criti()}><MdIcons.MdSpellcheck size="25" /> {props.critation_name ? <> {props.critation_name.substring(0, limit)} {limit <= length_check ? '...' : ''} </> : ''}
                                </h5>

                            </Col>
                        </OverlayTrigger>
                        <Col lg={{ size: 4 }} xs={{ size: 12 }} className="easy-ref-blue-btn easy-ref-titles" onClick={handleShow_edit}>
                            <h5 className="text-white m-0">Add New reference <AiIcons.AiOutlinePlusCircle /></h5>

                        </Col>
                    </Row>
                </div>
                <div className="easy-ref-actions">
                    <Row className="m-0 align-items-center">
                        <Label className="easy-ref-form-title">
                            <Input type="checkbox" className=" checkbox_size m-l-auto" name="selectAll"
                                id="selectAll" onChange={handleSelectAll}
                                checked={isCheckAll} />{' '}
                            <span className="easy-ref-check">
                                <i>&#10003;</i>
                            </span>
                        </Label>
                        <div className="d-flex align-items-center search-action">
                            <div className="search-input">
                                <input type="text" className="form-control" placeholder="Search..." value={search_data} onChange={(e) => setsearch_data(e.target.value)} />
                            </div>
                            <div className="easy-ref-button-groups">
                                <button className="btn btn-info p-l-10" onClick={() => share('off')}><FaIcons.FaShare />  Share</button>
                                <button className="btn btn-danger p-l-10" onClick={() => handleShow_delete_multiple()}><AiIcons.AiFillDelete />  Delete</button>
                                <button className="btn btn-warning" onClick={() => handleshowMoveRef()}><MiIcons.MdContentCut />  Move</button>
                                <button className="btn btn-dark p-l-10" onClick={() => copy_ref()}><HiIcons.HiOutlineDuplicate />  Duplicate</button>
                                <button className="btn btn-secondary p-l-10" onClick={() => copy_references()}><HiIcons.HiOutlineClipboardCopy />  Copy</button>
                                <button className="btn btn-success p-l-10" onClick={() => downloadTxtFile()}><AiIcons.AiOutlineDownload />  Download</button>
                            </div>
                        </div>
                    </Row>
                </div>
                <div className="easy-ref-ticket-list">
                    {
                        props.reference_list && props.reference_list.filter(person => person.formatted_data.includes(search_data)).map((ref) => (
                            <div className="easy-ref-tickets">
                                <Row className="m-0 align-items-start easy-ref-card">
                                    <Label className="easy-ref-form-title">
                                        <Input type="checkbox" className="m-l-auto checkbox_size" id=
                                            {ref.id} onChange={handleClick}
                                            checked={isCheck.includes(ref.id)} />{' '}
                                        <span className="easy-ref-check">
                                            <i>&#10003;</i>
                                        </span>
                                    </Label>
                                    <div className="easy-ref-card-detail">
                                        <div className="easy-ref-block">
                                            <h5 className="easy-ref-ticket-title">In-text Citation</h5>
                                            <p className="easy-ref-ticket-sub">{ReactHtmlParser(ref.intext_citation_data)}</p>
                                        </div>
                                        <div className="easy-ref-block mb-0">
                                            <h5 className="easy-ref-ticket-title">End reference</h5>
                                            <p className="easy-ref-ticket-sub">{ReactHtmlParser(ref.formatted_data)}</p>
                                        </div>
                                        <Dropdown className="nav-main dropdown_set">
                                            <MdIcons.MdContentCopy size="30" onClick={() => copyToClipboardText(ref.intext_citation_data, ref.formatted_data)} />
                                            <Dropdown.Toggle id="dropdown-basic" className="p-0">
                                                <MiIcons.MdMoreVert size="30" />
                                            </Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                <Dropdown.Item className="drop-down-menus" onClick={() => copyToClipboard(ref.formatted_data)}><span><HiIcons.HiMenuAlt2 size="20" color="black" /></span><span className="m-l-5" >Copy bibliography entry</span></Dropdown.Item>
                                                <Dropdown.Item className="drop-down-menus" onClick={() => copyToClipboardText(ref.intext_citation_data)}><span><FaIcons.FaQuoteRight size="20" color="black" /></span><span className="m-l-5" >Copy in-text citation</span></Dropdown.Item>
                                                <Dropdown.Item className="drop-down-menus" onClick={() => move_ref(ref.id, ref.project_id)}><span><MiIcons.MdContentCut size="20" color="black" /></span><span className="m-l-5" >Move to Project...</span></Dropdown.Item>
                                                <Dropdown.Item className="drop-down-menus" onClick={() => copy_ref(ref.id, ref.project_id)}><span><HiIcons.HiOutlineDuplicate size="30" color="black" /></span><span className="m-l-5" >Copy to Project...</span></Dropdown.Item>
                                                <Dropdown.Item className="drop-down-menus"><span><MiIcons.MdEdit size="20" color="black" /></span><span className="m-l-5" >Edit</span></Dropdown.Item>
                                                <Dropdown.Divider className="m-0" />
                                                <Dropdown.Item className="error text-center drop-down-menus" onClick={() => handleShow_delete_single(ref.id, ref.project_id)}><span><AiIcons.AiFillDelete size="20" color="red" /></span><span className="m-l-5 font_size" >Delete</span></Dropdown.Item>
                                                {/* <Dropdown.Item className="error" onClick={() => ref_delete(ref.id, ref.project_id)}><span><AiIcons.AiFillDelete size="20" color="red" /></span><span className="m-l-5 font_size" >Delete</span></Dropdown.Item> */}
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </div>
                                </Row>
                            </div>
                        ))
                    }
                </div>

                <div className="m-b-15 p-r-15  p-l-15 d-flex justify-content-end easy-ref-btn-group">
                    <div className="p-r-10 ">
                        <Button variant="secondary" disabled={props.current_page === 0 ? 'disabled' : ''} onClick={() => PreviusPage(props.current_page)}>Previus Page</Button>
                    </div>
                    <div>
                        <Button variant="info" disabled={props.next_page ? '' : 'disabled'} onClick={() => nextPage(props.current_page)}>Next Page</Button>
                    </div>
                </div>

                <Reference ref_model={ref_model} handleShow_edit={() => handleShow_edit()} />

            </div>
            <Modal show={show} onHide={handleClose} aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Select Move Project</Modal.Title>
                </Modal.Header>
                <Modal.Body className="height_set">
                    <ul className="remove_dot p-0 ">
                        {
                            props.project_list && props.project_list.map((project) => (
                                props.project_ids != project.id ?
                                    <li className="p-3 hover_class" onClick={() => move_reference(project.id)} >{project.name}</li> : ''
                            ))
                        }
                    </ul>
                </Modal.Body>
            </Modal>
            <Modal show={ref_mul_move} onHide={handleCloseMoveRef} aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Select Move Project</Modal.Title>
                </Modal.Header>
                <Modal.Body className="height_set">
                    <ul className="remove_dot p-0 ">
                        {
                            props.project_list && props.project_list.map((project) => (
                                props.project_ids != project.id ?
                                    <li className="p-3 hover_class" onClick={() => move_ref_multiple(project.id)} >{project.name}</li> : ''
                            ))
                        }
                    </ul>
                </Modal.Body>
            </Modal>
            <Modal show={ref_mul_copy} onHide={handleCloseCopyRef} aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Select Copy Project</Modal.Title>
                </Modal.Header>
                <Modal.Body className="height_set">
                    <ul className="remove_dot p-0 ">
                        {
                            props.project_list && props.project_list.map((project) => (
                                <li className="p-3 hover_class" onClick={() => copy_ref_multiple(project.id, project.name)} >{project.name}</li>
                            ))
                        }
                    </ul>
                </Modal.Body>
            </Modal>
            <Modal show={show_delete_multiple} onHide={handleClose_delete_multiple}>
                <Modal.Header closeButton>
                    <Modal.Title>Reference Delete</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure, you want to remove this references?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose_delete_multiple}>
                        No
                    </Button>
                    <Button variant="danger" onClick={() => delete_ref_multiple()}>
                        Yes
                    </Button>
                </Modal.Footer>
            </Modal>
            <Modal show={show_delete_single} onHide={handleClose_delete_single}>
                <Modal.Header closeButton>
                    <Modal.Title>Reference Delete</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure, you want to remove this reference?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose_delete_single}>
                        No
                    </Button>
                    <Button variant="danger" onClick={() => ref_delete()}>
                        Yes
                    </Button>
                </Modal.Footer>
            </Modal>
            <Modal className="change-style-model" show={show_criti} onHide={handleClose_criti}>
                <Modal.Header closeButton>
                    <Modal.Title>Change Style</Modal.Title>
                </Modal.Header>
                <Modal.Body className="height_set">
                    <input className="form-control" type="text" placeholder="search..." value={search_criti} onChange={(e) => setsearch_criti(e.target.value)} onClick={Select_toggle} required />
                    <ul className='visible select_tag m-0 p-0'>
                        <p className="font-weight-bold  bg-color m-t-10 p-2">Popular style</p>
                        {
                            props.citation_popular && props.citation_popular.filter(person => person.title.includes(search_criti)).map((popular) => (

                                <li className="style-list citation_font" onClick={() => selectCritation(popular.id, popular.title)}>{popular.title}</li>

                            ))
                        }
                        <span className="bg-color d-flex justify-content-between align-items-center">
                            <p className="font-weight-bold  m-0  p-2">Regular style</p>
                            <a href="https://citationstyles.org/" target="_blank"><img className="image_size_csl  m-r-10" src={Csl} /></a>
                        </span>
                        {
                            props.citation_regular && props.citation_regular.filter(person => person.title.includes(search_criti)).map((popular) => (

                                <li className="style-list citation_font" onClick={() => selectCritation(popular.id, popular.title)}>{popular.title}</li>

                            ))
                        }
                    </ul>

                </Modal.Body>
            </Modal>
            <Modal show={show_share_email} onHide={handleClose_share_email} size="lg" aria-labelledby="contained-modal-title-vcenter">
                <Modal.Header closeButton>
                    <Modal.Title>Share Reference</Modal.Title>
                </Modal.Header>
                <form onSubmit={(values) => send_email(values)}>
                    <Modal.Body >
                        <Col lg={{ size: 12 }} xs={{ size: 12 }} className="m-b-10">
                            <h5><b>Include in-text Citation?</b> <label class="switch m-l-15" >
                                <input type="checkbox" onChange={() => in_text()} />
                                <span class="slider round"></span>
                            </label></h5>
                        </Col>
                        <Col xs={{ size: 12 }} lg={{ size: 12 }} className="share_m_height_set">
                            {
                                ReactHtmlParser(share_Data)
                            }
                        </Col>
                        <Col xs={{ size: 12 }} lg={{ size: 12 }} className="position-fix border_top">
                            <label className="font-weight-bold m-t-10">Email</label>
                            <input className="form-control" type="email" name="share_email" value={send_email_project} onChange={(e) => setsend_email_project(e.target.value)} placeholder="Email Id" required></input>
                        </Col>
                    </Modal.Body>
                    <Modal.Footer className="justify-content-center">
                        {/* <Button variant="secondary" onClick={handleClose_share_email}>
                            Close
                    </Button> */}
                        <Button variant="info" type="submit">
                            Send Email
                    </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>

    )
}
const mapStateToProps = (state) => {
    const { login, sidebar, project, Dashboard } = state;
    return { ...login, ...sidebar, ...project, ...Dashboard };
};
export default connect(mapStateToProps, null)(Live);
