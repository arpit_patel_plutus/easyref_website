import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import { PROJECT_ERROR, REFERENCELISTGET, SHOW_LODER, REFERENCEMOVE, PROJECT_LIST,REFERENCEMOVEMULTIPLE,REFERENCECOPY } from '../../actions/constants';
import { apiUrl } from '../../environment';

const getUsersStats = (RefData) => axios.get(`${apiUrl}references/edit/?id=${RefData.reference_id}&project_id=${RefData.project_id}`);
const getMoveRef = (move_ref_data) => axios.post(`${apiUrl}references/edit/?ids=${move_ref_data.ids}&project_id=${move_ref_data.move_pro_id}`);
const getProjectList = (ids) => axios.get(`${apiUrl}users/projects/${ids}`);
const getReferences = (ref_data) => axios.get(`${apiUrl}references/?project_id=${ref_data.id}&page=${ref_data.page}&per_page=${ref_data.per_page}`);
const getCopyRef = (copy_ref_Data) => axios.post(`${apiUrl}references/duplicate/?ids=${copy_ref_Data.ids}&project_id=${copy_ref_Data.copy_project_id}`);

function* getProjectlistSaga(RefData) {
    try {
        const response = yield call(getUsersStats, RefData);
        if (response.data.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            const projectList = yield call(getProjectList, obj.id);
            if (projectList.data.errorCode === 0) {

                yield put({ type: PROJECT_LIST, project_list: projectList.data });
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            const ref_data = { id: RefData.old_pro_id, page: '1', per_page: '15' };
            const ref_response = yield call(getReferences, ref_data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list: ref_response.data ,next_page:ref_response.data.next_page, current_page: ref_response.data.current_page  - 1 , project_ids : Number(RefData.old_pro_id)});

            }
            yield put({ type: SHOW_LODER, isLoading: false });
        }
        else {
            yield put({ type: SHOW_LODER, isLoading: false });
            // yield put({ type: PROJECT_ERROR, response });
        }
    } catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* getMoveRefSaga (move_ref_data) {
    try {
        const response = yield call(getMoveRef, move_ref_data);
        if (response.data.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            const projectList = yield call(getProjectList, obj.id);
            if (projectList.data.errorCode === 0) {

                yield put({ type: PROJECT_LIST, project_list: projectList.data });
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            const ref_data = { id: move_ref_data.project_id, page: '1', per_page: '15' };
            const ref_response = yield call(getReferences, ref_data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list: ref_response.data ,next_page:ref_response.data.next_page, current_page: ref_response.data.current_page  - 1 , project_ids : Number(move_ref_data.project_id)});

            }
            yield put({ type: SHOW_LODER, isLoading: false });
        }
        else {
            yield put({ type: SHOW_LODER, isLoading: false });
            // yield put({ type: PROJECT_ERROR, response });
        }
        
    } catch (error) {
        
    }
}
function* copyReference(copy_ref_Data)
{
    try {
        console.warn(copy_ref_Data)
        const response = yield call(getCopyRef, copy_ref_Data);
        if (response.data.errorCode === 0) 
        {
            var user_id = localStorage.getItem("user");
            var obj = JSON.parse(user_id);
            const projectList = yield call(getProjectList, obj.id);
            if (projectList.data.errorCode === 0) {

                yield put({ type: PROJECT_LIST, project_list: projectList.data });
            } else {
                yield put({ type: PROJECT_ERROR, response });
            }
            const ref_data = { id: copy_ref_Data.project_id, page: '1', per_page: '15' };
            const ref_response = yield call(getReferences, ref_data);
            if (ref_response.data.errorCode === 0) {
                yield put({ type: REFERENCELISTGET, ref_list: ref_response.data ,next_page:ref_response.data.next_page, current_page: ref_response.data.current_page  - 1 , project_ids : Number(copy_ref_Data.project_id)});

            }
            yield put({ type: SHOW_LODER, isLoading: false });
        }
        else {
            yield put({ type: SHOW_LODER, isLoading: false });
            // yield put({ type: PROJECT_ERROR, response });
        }
        
    } catch (error) {
        
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(REFERENCEMOVE, getProjectlistSaga)]);
    yield all([takeLatest(REFERENCEMOVEMULTIPLE, getMoveRefSaga)]);
    yield all([takeLatest(REFERENCECOPY, copyReference)]);
}

export default watchUserAuthentication;