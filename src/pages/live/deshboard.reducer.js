import * as types from '../../actions/constants';

const initialState = {
    token: 'HaGBYHqP4sbXght57wBfgNTkFf9JPDeH',
    
};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.REFERENCELIST:
            return { ...state, cread: action.user };
        case types.REFERENCELISTGET:
            return { ...state, reference_list: action.ref_list.data,next_page:action.next_page,current_page:action.current_page, project_ids:action.project_ids };
        case types.USER_LOGIN_ERROR:
            return { ...state, response: action.response.data };
        case types.SHOW_LODER:
            return { ...state, isLoading: action.isLoading };
            case types.PROJECTNAME:
            return { ...state, project_name: action.project_name, critation_name:action.critation_name };
        default:
            return state;
    }
}