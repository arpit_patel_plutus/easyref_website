import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import * as types from '../../actions/constants';
import { apiUrl } from '../../environment';

// const authenticateUser = (user) => axios.post(`${apiUrl}/auth/clientUserLogin`, user);

const resetUserPassword = (user) => axios.get(`${apiUrl}users/get_password?email=${user.email.toLowerCase()}`);

function* resetPAsswordSaga({user, history}) {
    try {
        const response = yield call(resetUserPassword, user);
        if(response.data.errorCode === 0){
            yield put({ type: types.USER_PASSWORD,reset_success: response.data});
            yield put({ type: types.SHOW_LODER, isLoading: false });
            // history.push('/login');
        }else{
            yield put({ type: types.SHOW_LODER, isLoading: false });
            yield put({ type: types.USER_RESET_ERROR, response });
        }
    } catch (error) {
        // yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchResetPassword() {
    yield all([takeLatest(types.USER_RESET_PASSWORD, resetPAsswordSaga)]);
}

export default watchResetPassword;