import * as types from '../../actions/constants';

const initialState = {
    token: 'HaGBYHqP4sbXght57wBfgNTkFf9JPDeH',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.USER_PASSWORD:
            return { ...state, reset_success: action.reset_success };
            case types.USER_RESET_ERROR:
            return { ...state, reset_error: action.response.data };
        default:
            return state;
    }
}