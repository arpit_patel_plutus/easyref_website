import React, { Component, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { LocalForm, Control } from 'react-redux-form';
// import { Link } from 'react-router-dom'
import { validateEmail } from '../../utilities/regex';
import { required } from '../../utilities/regex';
// import { Button, Row, Col, FormGroup, Form, FormFeedback, FormText, Label, Input } from 'reactstrap';
import { resetPasswordAction , setLoading} from "./../../actions/user/index";
import "./reset-password.scss";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import FullPageLoader from "../../components/FullPageLoader/loder";


export class ResetPassword extends Component {
    // constructor(props) {
    //     super(props);
    // }
    render() {
        const data = { ...this.props, ...this.state };
        return (<ResetPasswordForm {...data} />);
    }
}
// const element = <FontAwesomeIcon icon={faCoffee} />
const ResetPasswordForm = (props) => {
    const dispatch = useDispatch();
    const reducerObject = useSelector(state => state);
    const [emailError, setEmailError] = useState('')
    const onHandleResetPassword = (loginData) => {
        var email = validateEmail(loginData.email)
        if (email === true) {
          setEmailError('')
          dispatch(setLoading(true))
          loginData = { ...loginData, token: reducerObject.login.token }
          dispatch(resetPasswordAction(loginData, props.history))
          
        } else {
          setEmailError('Enter valid Email !')
        }
    }
    const validateEmail1 = (e) => {
        var email = e.target.value
        var email = validateEmail(email)
        if (email === true) {
          setEmailError('')
        } else {
          setEmailError('Enter valid Email !')
        }
      }
    return (
        <>
        { props.login.isLoading ? <FullPageLoader /> : <></> }
        <div className="limiter">
            <div className="container-login100">
                <div className="wrap-login100">
                <LocalForm className="login100-form validate-form" onSubmit={(values) => onHandleResetPassword(values)} model="user">
                    {/* <form className="login100-form validate-form"> */}
                        <div className="login100-form-avatar text-center">
                            {/* <img src={UserIcon} alt="" /> */}
                            <h3 className="logo m-0">Easy Referencing</h3>
                            <h1 className="fpsc-title2">Forgot password</h1>
                        </div>
                        <span className="login100-form-title p-t-10 p-b-10">
                        {props.reset_error ? <div>{props.reset_error.errorMessages}</div> : ''}
                        {/* {props.reset_success ? <div>{props.reset_success.message}</div> : ''} */}
                        </span>
                        <span className="reset100-form-title p-b-10">
                            {props.reset_success ? <div>{props.reset_success.message}</div> : ''}
                        </span>
                        <div className="wrap-input100 validate-input m-b-10">
                            {/* <input className="input100" type="text" name="email" placeholder="Email address" /> */}
                            <Control.text
                                type="email"
                                model=".email"
                                className={emailError ? ' login-inputs form-control input100 error' : "login-inputs form-control input100"}
                                id="email"
                                placeholder="Email"
                                onChange={(e) => validateEmail1(e)}
                            />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100 m-l-0">
                                {/* <i class="fa fa-user" aria-hidden="true"></i> */}
                                <FontAwesomeIcon icon={faUser} />
                            </span>
                        </div>
                        <div className="error-text m-b-10">{emailError}</div>
                        <div className="container-login100-form-btn p-t-10">
                            <button type="submit" className="login100-form-btn">Send</button>
                        </div>
                        <div className="text-center w-full p-t-25 reset-pwd-div">
                            Have an account already? <NavLink to="login" className="reset-pwd"> Log in </NavLink>
                        </div>
                    {/* </form> */}
                    </LocalForm>
                </div>
            </div>
        </div>
        </>
    )
}

const mapStateToProps = (state) => {
    const { login, sidebar , resetpassword } = state;
    return { login, ...sidebar , ...resetpassword};
};

export default connect(mapStateToProps, null)(ResetPassword);
