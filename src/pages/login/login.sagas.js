import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from "axios";
import * as types from '../../actions/constants';
import { apiUrl } from '../../environment';

const authenticateUser = (user) => axios.get(`${apiUrl}users/login?email=${user.email.toLowerCase()}&password=${user.password}&type=email`);
const getUsersStats = (user) => axios.get(`${apiUrl}users/stats/${user.id}`);

function* loginSaga({ user, history }) {
    try {
        const response = yield call(authenticateUser, user);
        if (response.data.errorCode === 0) {
            localStorage.setItem('user', JSON.stringify(response.data.data));
            yield put({ type: types.USER_LOGIN_SUCCESS, user: response.data.data });
            var user = localStorage.getItem("user");
            var obj = JSON.parse(user);
            const response_stats = yield call(getUsersStats, obj);
            if (response_stats.data.errorCode === 0) {
                let total_ref = 0;
                response_stats.data.data.forEach(pref => { total_ref += Number(pref.reference_count) });
                yield put({ type: types.SET_STATS, stats: response_stats.data.data, total_ref });
            } else {
                // yield put({ type: types.USER_LOGIN_ERROR, response });
            }
            history.push('/live');
        } else {
            yield put({ type: types.SHOW_LODER, isLoading: false });
            yield put({ type: types.USER_LOGIN_ERROR, response });
        }
    } catch (error) {
        yield put({ type: types.USER_LOGIN_ERROR, error });
    }
}

function* watchUserAuthentication() {
    yield all([takeLatest(types.USER_LOGIN, loginSaga)]);
}

export default watchUserAuthentication;