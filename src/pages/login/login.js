import React, { Component, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { LocalForm, Control } from 'react-redux-form';
import { validateEmail } from '../../utilities/regex';
import { loginUserAction, setLoading } from "../../actions/user/index";
import "./login.scss";
import { faLock, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import FullPageLoader from "../../components/FullPageLoader/loder";

export class Login extends Component {

    render() {
        const data = { ...this.props, ...this.state };
        return (<LoginForm {...data} />);
    }
}

const LoginForm = (props) => {
    const dispatch = useDispatch();
    const reducerObject = useSelector(state => state);
    const [emailError, setEmailError] = useState('')
    const [passwordError, setPasswordError] = useState('')
    const onHandleLogin = (loginData) => {
        var email = validateEmail(loginData.email)
        console.log(email)
        var passwordempty =true;
        if (email === true) {
          setEmailError('')
          
        } else {
          setEmailError('Enter valid Email !')
        }
        if(!loginData.password)
        {
            var passwordempty = false;
            setPasswordError('Enter password !')
        }
        else
        {
            setPasswordError('')
        }

        if(email === true && passwordempty === true)
        {
            dispatch(setLoading(true))
            loginData = { ...loginData, token: reducerObject.login.token }
            dispatch(loginUserAction(loginData, props.history))
        }
    }
    const validateEmail1 = (e) => {
        var email = e.target.value
        var email = validateEmail(email.toLowerCase())
        if (email === true) {
          setEmailError('')
        } else {
          setEmailError('Enter valid Email !')
        }
      }
      const validatepassword = (e) => {
        var password = e.target.value
        if (password !== '') {
            setPasswordError('')
        } else {
            setPasswordError('Enter Password !')
        }
      }

    return (
        <>
            { props.login.isLoading ? <FullPageLoader /> : <></>}
            <div className="limiter">
                <div className="container-login100">
                    <div className="wrap-login100">
                        <LocalForm className="login100-form validate-form"  onSubmit={(values) => onHandleLogin(values)} model="user">
                            <div className="login100-form-avatar">
                                <h1 className="logo m-0">Easy Referencing</h1>
                            </div>
                            <span className="login100-form-title p-b-45">
                                {props.login.response ? <div>{props.login.response.errorMessages}</div> : ''}
                            </span>
                            <div className="wrap-input100 validate-input m-b-10">
                                <Control.text
                                    type="text"
                                    model=".email"
                                    className={emailError ? ' login-inputs form-control input100 error' : "login-inputs form-control input100"}
                                    id="email"
                                    placeholder="Email"
                                    onBlur={(e) => validateEmail1(e)}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon={faUser} />
                                </span>
                            </div>
                            <div className="error-text m-b-10">{emailError}</div>
                            <div className="wrap-input100 validate-input m-b-10">
                                <Control.text
                                    type="password"
                                    model=".password"
                                    className={passwordError ? ' login-inputs form-control input100 error' : "login-inputs form-control input100"}
                                    id="password"
                                    placeholder="Password"
                                    onBlur={(e) => validatepassword(e)}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon={faLock} />
                                </span>
                            </div>
                            <div className="error-text m-b-10">{passwordError}</div>
                            <div className="container-login100-form-btn p-t-10">
                                <button className="login100-form-btn"  type="submit">Log in</button>
                            </div>
                            <div className="text-center w-full p-t-25 reset-pwd-div">
                                Forgot Password? <NavLink to="reset-password" className="reset-pwd">Reset it here </NavLink>
                            </div>
                        </LocalForm>
                    </div>
                </div>
            </div>
        </>
    )
}
const mapStateToProps = (state) => {
    const { login, sidebar, resetpassword } = state;
    return { login, ...sidebar, ...resetpassword };
};
export default connect(mapStateToProps, null)(Login);
