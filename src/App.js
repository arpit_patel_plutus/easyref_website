import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.scss';
import './css/animate.css';
// import './css/font-awesome.min.css';
import './css/icon-font.min.css';
import './css/util.css';
import withAUth from "./components/withAuth";
// import { ConnectedRouter } from 'connected-react-router';
import { routes, protectedroutes } from "./routes";
import { history } from './main/history';


function App() {
    return (
      <Router history={history}>
          <Suspense fallback={<div>Loading..</div>}>
            <Switch>
              {protectedroutes.map((route, index) => (
                <Route key={index} path={route.path} exact={route.exact} component={() => withAUth(route.component)} ></Route>
              ))}
              {routes.map((route, index) => (
                <Route key={index} path={route.path} exact={route.exact} component={route.component} ></Route>
              ))}
            </Switch>
          </Suspense>
        </Router>
    );
}

export default App;
