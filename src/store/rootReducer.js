import { combineReducers } from "redux";
import loginReducer from "../pages/login/login.reducer";
import profileReducer from "../components/profile/profile.reducer";
import projectReducer from "../components/newsidebar/Navbar.reducer";
import DashboardReducer from "../pages/live/deshboard.reducer";
import resetReducer from "../pages/reset-password/reset-password.reducer";
import { connectRouter } from 'connected-react-router';
import { history } from '../main/history';

const rootReducer = combineReducers({
  login: loginReducer,
  sidebar: profileReducer,
  resetpassword: resetReducer,
  project: projectReducer,
  Dashboard: DashboardReducer,
  
  router: connectRouter(history),
});

export default rootReducer;