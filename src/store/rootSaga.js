import { all, fork } from "redux-saga/effects";

import loginSaga from "../pages/login/login.sagas";
import DashboardSaga from "../pages/live/dashboard.sagas";
import RefDelete from "../pages/live/referenceDelete.sagas";
import RefMove from "../pages/live/referenceMove.sagas";
import resetPwdSaga from "../pages/reset-password/reset-password.sagas";
import profileSaga from "../components/profile/profile.sagas";
import projectSaga from "../components/newsidebar/Navbar.sagas";
import projectSaveSaga from "../components/newsidebar/project.sagas";
import projectDeleteSaga from "../components/newsidebar/projectdelete.sagas";
import projectEditSaga from "../components/newsidebar/projectedit.sagas";

export function* rootSaga() {
  yield all([fork(loginSaga), fork(resetPwdSaga), fork(profileSaga),fork(projectSaga),fork(projectSaveSaga),fork(projectDeleteSaga),fork(projectEditSaga),fork(DashboardSaga),fork(RefDelete),fork(RefMove)]);
}