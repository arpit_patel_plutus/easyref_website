# Getting Started with Offsight console

### npm install

## Available Scripts

In the project directory, you can run:

### `npm start`

## Note:

1.) Every new url / route will be a new file in pages directory.  
2.) All subcomponents which are specific to that page will be in that pages directory.  
3.) All components which are used across multiple components will be inside components dir.  
4.) Use BEM naming for css.